package com.satya.concurrent;

import java.util.concurrent.locks.ReentrantLock;

import multithreading.basics.locks.Lock;
import multithreading.basics.locks.Locks;

public class Counter {
	private int count;
	private Lock lock;

	public Counter(int cnt) {
		count = cnt;
		lock = new Lock(){
			ReentrantLock lock = new ReentrantLock();
			public void lock(){ lock.lock(); }
			public void unlock(){ lock.unlock(); }
		};
	}

	public int val() {
		try {
			lock.lock();
			int ret = count;
			return ret;
		} catch (InterruptedException e) {
			return count;
		} finally {
			lock.unlock();
		}
	}

	public int getAndIncValue() {
		try {
			lock.lock();
			int tmp = count;
			this.count = tmp + 1;
			return tmp;
		} catch (InterruptedException e) {

		} finally {
			lock.unlock();
		}
		return count;
	}

	void setLock(Lock lock) {
		this.lock = lock;
	}

}
