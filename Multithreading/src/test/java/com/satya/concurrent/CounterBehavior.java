package com.satya.concurrent;

import static com.satya.common.assertions.BehavioralAssertions.*;

import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

import org.jmock.lib.concurrent.Blitzer;
import org.junit.After;
import org.junit.Test;

import multithreading.basics.locks.FilterLock;
import multithreading.basics.locks.Lock;
import multithreading.basics.locks.Locks;

public class CounterBehavior {
	
	Blitzer blitzer = new Blitzer(25000);
	
//	@Test
	public void test1() throws InterruptedException {
		Counter counter = new Counter(0);
		Runnable run = ()->{
			int val = counter.getAndIncValue();
//			calling(counter.val()).shouldReturn(greaterThan(val));
		};
		blitzer.blitz(run);
		
		calling(blitzer.totalActionCount()).shouldReturn(counter.val());
		
	}
	
	@Test//(expected=AssertionError.class)
	public void counter_should_increase_exclusively_by_threads(){
		
		Counter counter = new Counter(0);
//		counter.setLock(new Lock(){});
		
		loopOver(0, 200, x->{
			Consumer<Integer> consumer = i -> loopOver(0, 5, j-> counter.getAndIncValue());
			
			runConcurrently(consumer).withNumberOfThreadsEqualTo(10);
			
			calling(counter.val()).shouldReturn((x+1)*50);
			sleep();
		});
		
	}
	
	private void sleep() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() throws InterruptedException {
	    blitzer.shutdown();
	}
}
