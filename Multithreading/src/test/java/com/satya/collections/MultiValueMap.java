package com.satya.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MultiValueMap<K, V> {

	private Map<K, List<V>> _map = new HashMap<K, List<V>>();

	public int getSize() {
		return _map.size();
	}

	public List<V> getValues(K key) {
		List<V> values = _map.get(key);
		if (values == null) {
			values = new ArrayList<V>();
		}

		return Collections.synchronizedList(values);

	}

	Lock lock = new ReentrantLock();

	public void put(K key, V value) {
		lock.lock();
		try {
			putValue(key, value);
		} finally {
			lock.unlock();
		}
	}

	protected void putValue(K key, V value) {
		List<V> values = _map.get(key);
		if (values == null) {
			values = new ArrayList<V>();
			_map.put(key, values);
		}
		values.add(value);
	}

	protected void setLock(Lock lock) {
		this.lock = lock;
	}
}
