package com.satya.collections;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Before;
import org.junit.Test;

import com.satya.common.assertions.BehavioralAssertions.Matcher;

import static com.satya.common.assertions.BehavioralAssertions.*;

public class MultiValueMapTest {

	MultiValueMap<String, String> _map;

	@Before
	public void setUp() throws Exception {
		_map = new MultiValueMap<String, String>();
	}

	@Test
	public void testMapEmptyUponCreate() {
		calling(_map.getSize()).shouldReturn(0);
	}

	@Test
	public void testGetValueForNonExistentKey() {
		calling(_map.getValues("nope")).shouldReturn(anEmptyList());
	}

	@Test
	public void testPutOneValueForAKey() {
		_map.put("1", "one");
		calling(_map.getValues("1")).shouldReturn(listWithSize(1));
	}

	@Test
	public void testPutValueForAnotherKey() {
		_map.put("2", "two");
		calling(_map.getValues("2").get(0)).shouldReturn("two");
	}

	@Test
	public void testTwoValuesForOneKey() {
		_map.put("1", "one");
		_map.put("1", "uno");

		calling(_map.getValues("1")).shouldReturn(listWithSize(2));
		calling(_map.getValues("1")).shouldReturn(
				listWithItems(inOrder(), "uno", "one"));
	}

	@Test
	public void testCheckSizeAfterPuts() {
		_map.put("1", "one");
		_map.put("2", "two");
		_map.put("1", "uno");

		calling(_map.getSize()).shouldReturn(2);
	}

	@Test
	public void testEnsureGetValuesReturnsSynchronizedList() {
		_map.put("1", "one");

		calling(_map.getValues("1")).shouldReturn(
				anObjectOfType(Collections.synchronizedList(EMPTY_LIST)
						.getClass()));

	}

	
}
