package multithreading.basics;

public class ThreadSafeSingletonExample {

	// Java Memory Model --> Describes what can you expect from incorrect synchronization and how can you
	// correctly synchronize without knowing underlying system architecture.
	// Out-of-order execution when you are not synchronizing.
	// Compiler Optimizations -- Compiler may eliminate fetch instruction (Elimination of read operation if it is
	// preceded by Read/Write operation on the same variable.
	// Volatile impact: Guarantees order and happens-before for reads w.r.t writes. Data will be loaded from Main memory only.
	// Happens-Before MM:  
	
	
	class UnsafeSingleton{
		protected UnsafeSingleton instance;
		private UnsafeSingleton(){ }
		
		public UnsafeSingleton getInstance_1(){
			if(instance == null) { instance = new UnsafeSingleton();}
			return instance;
		}
		
		public UnsafeSingleton getInstance_2(){
			if(instance == null) { 
				synchronized (getClass()) {
					instance = new UnsafeSingleton();}}
			return instance;
		}
		
		public UnsafeSingleton getInstance_DCL(){
			/** 
			 * 
			 */
			if(instance == null) { 
				synchronized (getClass()) {
					if(instance == null) // For thread2 this might be not null, but at the same time 
										 // the object may not have been fully initialized.
										 // JIT might replace constructor with the inline code for intialization
										 // and there is no ordering gaurantee for object creation code and 
										 // instance assignment code.
						
						instance = new UnsafeSingleton();}}
			return instance;
		}
	}

	class SafeSingletonWithVolatile extends UnsafeSingleton{
		volatile SafeSingletonWithVolatile instance; // Just use volatile for instance.
	}
}

class SafeSingletonWithHolder{
	/**
	 *  JVM guarantees that operations that are part of class initialization are
		guaranteed to be visible to all threads that use that class. Because an inner class is not loaded until
		some thread references one of its fields or methods, we get a lazy initialization. 
	 */
	static class Holder{
		public static SafeSingletonWithHolder instance = new SafeSingletonWithHolder();
	}
	
	public SafeSingletonWithHolder get(){
		return Holder.instance;
	}
}