package multithreading.basics;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class DiningPhilosopher {

	public static void main(String[] args) {
		int total = 10;

		Chopstick[] chopsticks = IntStream.range(1, total).mapToObj(i -> new Chopstick(i)).toArray(Chopstick[]::new);

		IntStream.range(1, total).forEach(i -> new Philosopher2(chopsticks[i], chopsticks[(i + 1) % total]).start());
	}

	/**
	 * Deadlock is possible here.
	 *
	 */
	static class Philosopher extends Thread {
		protected Chopstick left, right;
		protected Random random;

		public Philosopher(Chopstick left, Chopstick right) {
			this.left = left;
			this.right = right;
			random = new Random();
		}

		public void run() {
			try {
				while (true) {
					thinkForSometime();
					eat();
				}
			} catch (InterruptedException e) {
			}
		}

		private void thinkForSometime() throws InterruptedException {
			Thread.sleep(random.nextInt(10));
			System.out.println(Thread.currentThread().getId() + " is thinking");
		}

		protected void eat() throws InterruptedException {
			synchronized (left) {
				synchronized (right) {
					Thread.sleep(random.nextInt(10));
					System.out.println(Thread.currentThread().getId() + " is eating");
				}
			}
		}
	}

	/**
	 * A fixed Global order. But this mecahnism requires you to acuire locks all
	 * in one place. Which is not feasible in practical scenarios.
	 * 
	 * To break the deadlock. Use unique id for chopsticks. Lock only in
	 * increasing order of id. i.e. if left.id < right.id synchronized(left) {
	 * synchronized(right){...}} else synchronized(right) {
	 * synchronized(left){...}}
	 * Instead of id, hashCode can be used (which is mostly unique, but not guaranteed)
	 */
	static class Philosopher2 extends Philosopher {

		Chopstick first, second;
		public Philosopher2(Chopstick left, Chopstick right) {
			super(left, right);
			first = left.id < right.id ? left : right;
			second = left.id < right.id ? right : left;
		}

		protected void eat() throws InterruptedException {
			synchronized (first) {
				synchronized (second) {
					Thread.sleep (random.nextInt(10));
					System.out.println(Thread.currentThread().getId() + " is eating");
				}
			}
		}
	}

	static class Chopstick {
		public Chopstick(int i) {
			id = i;
		}
		int id;
	}
	
	/**
	 * Solution using ReentrantLocks and tryLock(timeout). This actually does
	 * not solve the problem. It just removes the possibility of Deadlock
	 * forever. It may also result in livelock, as all threads may fail at once,
	 * then try again, then fail again.
	 * While deadlocked threads in previous implementation can not be interrupted,
	 * this implementation allows to interrupt deadlocked threads.
	 *
	 */
	static class Philosopher3{}

	/**
	 * Solution using lock and conditions.
	 * 
	 */
	static class SmartPhilosopher {
		private boolean eating;
		private SmartPhilosopher left, right;
		private ReentrantLock table;
		private Condition condition;

		public SmartPhilosopher(ReentrantLock lock) {
			this.table = lock;
			condition = lock.newCondition();
		}

		public void run() {
			try {
				while (true) {
					think();
					eat();
				}
			} catch (Exception e) {
			}
		}

		private void eat() throws InterruptedException {
			table.lock(); // So that no other philosopher can change state, while it checks for condition.
			try {
				eating = false;
				left.condition.signal();
				right.condition.signal();
			} finally {
				table.unlock();
			}
			Thread.sleep (1000);
		}

		private void think() throws InterruptedException {
			table.lock();
			try {
				while(left.eating || right.eating)
					condition.await();
				eating = true;
				
			} finally {
				table.unlock();
			}
			Thread.sleep (1000);
		}
	}
	
	/**
	 * The same implementaion above can be rewritten using wait/notify calls.
	 * That implementation will be theoretically less efficient.
	 * 
	 */
}
	