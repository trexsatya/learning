package multithreading.basics.locks;

import static java.lang.Thread.currentThread;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Locks {

	public static Lock reentrantLock(){
		return new Lock(){

			private boolean isLocked;
			private Thread lockedBy;
			private int lockCount;
			Object sync = new Object();
			
			@Override
			public synchronized void lock() throws InterruptedException {
				synchronized (sync) {
					if(isLocked && lockedBy != currentThread()) sync.wait();
					
					isLocked = true; lockedBy = currentThread();
					lockCount ++;
				}
			}

			@Override
			public synchronized void unlock() {
				synchronized (sync) {
					if(!isLocked || lockedBy != currentThread()) return;
					
					lockCount--;
					if(lockCount == 0) {
						isLocked = false; 
						sync.notifyAll();
					}
				}
			}};}
	
	public static Lock flakyLock() {
		return new Lock() {
			volatile long turn ; volatile boolean busy;
			
			public void lock(){
				long me = Thread.currentThread().getId();
				do {
					do {
						turn = me;
					} while(busy);
					busy = true;
				} while( turn != me);
			}
			
			public void unlock(){
				busy = false;
			}
		};
	}
	
	public static Lock bakeryLock(){
		Function<Collection<Long>,Long> max = set -> {
			return set.stream().max((a,b) -> a.compareTo(b)).orElse(0l);
		};
		
		return new Lock(){
			Map<Long,Boolean> flag = new HashMap<>();
			Map<Long,Long> label = new HashMap<>();

			boolean lexicallySmallerLabel(long i, long j){
				return label(i) < label(j) ||
						(label(i) == label(j) && i < j);
			}
			
			BiPredicate<Set<Long>, Long> waitForLabel = (ids, id) -> {
				Optional<Long> exists = ids.stream().filter(i -> flag.get(i) && lexicallySmallerLabel(i, id)).findFirst();
				
				return exists.isPresent();
			};
			
			@Override
			public void lock() throws InterruptedException {
				long id = Thread.currentThread().getId();
				flag.put(id, true);
				Collection<Long> labels = label.values();
				
				label.put(id, max.apply(labels)+1 );
				Set<Long> ids = flag.keySet();
				ids.remove(id);
				while(waitForLabel.test(ids, id));
			}
			
			public void unlock() {
				flag.put(Thread.currentThread().getId(), false);
			}
			
			private long label(long id){ return Optional.ofNullable(label.get(id)).orElse(0l); }
		};
	}
}
