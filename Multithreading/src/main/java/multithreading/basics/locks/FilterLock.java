package multithreading.basics.locks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.satya.concurrent.Counter;

import static java.lang.Thread.*;

public class FilterLock implements Lock{

	Map<Long, Long> level = new HashMap<>();
	Map<Long, Long> victim= new HashMap<>();
	
	public void lock(){
		long me = currentThread().getId();
		int n = level.size();
		for(int i=1; i< n; i++){
			level(me,i);
			victim(i,me);
			
			int x = i;
			while(exists(k-> k != me && level(k) >= x && victim(x) == me )){}
		}
	}
	
	public void unlock(){
		long me = currentThread().getId();
		level(me,0);
	}
	
	private boolean exists(Predicate<Long> condition) {
		Set<Long> keySet = level.keySet();
		
		Stream<Long> filter = new HashSet<>(keySet).stream().filter(condition);
		return filter.findAny().isPresent();
	}

	public static void main(String[] args) {
		Lock lock = new FilterLock();
		
		Counter counter = new Counter(0);
		
	}
	
	public void level(long i, long x){ level.put(i, x); }
	public void victim(long i, long x){ victim.put(i, x); }
	
	public long level(long i){ return Optional.ofNullable(level.get(i)).orElse(0l); }

	public long victim(long i){ return Optional.ofNullable(victim.get(i)).orElse(0l); }
	
	
}

