package multithreading.basics.locks;

public interface Lock {
	default void lock() throws InterruptedException {}
	default void unlock(){}
}
