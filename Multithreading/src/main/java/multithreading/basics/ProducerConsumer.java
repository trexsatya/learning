package multithreading.basics;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class ProducerConsumer {

	/**
	 * Application:
	 * Count the most used word in wikipedia. Producer -> produces Page Queue
	 * maintains the pages, Counter map stores the frequences. Consumer(s)
	 * access the queue and count the word
	 * 
	 * Choices for Map: Synchronized map, ConcurrentMap, Choices for Queue:
	 * ArrayBlcokingQueue, ConcurrentLinkedBlockingQueue
	 * 
	 * Also each counter can keep a local map, and merge into master map.(less
	 * contention)
	 * 
	 * Work Stealing: Use ForkJoinPool instead of ExecutorService. Fork-Join can
	 * be used for those tasks also who don't need to combine; allows
	 * event-style tasks .
	 * 
	 * 
	 */

	public static void main(String[] args) {
//		new UsingLockConditions().run();
		new UsingSemaphores().run();
	}

	public void run() {
		
		Runnable producer = ()-> { while(true) produce(); };
		Runnable consumer = ()-> { while(true) consume(); };
		
		IntStream.range(0, 5).forEach(i -> {
			new Thread(producer).start();
		});

		IntStream.range(0, 3).forEach(i -> {
			new Thread(consumer).start();
		});
	}

	Queue<String> queue = new LinkedList<>();
	Random random = new Random();
	
	@SuppressWarnings("unused")
	private static class UsingLockConditions extends ProducerConsumer {
		
		Lock lock = new ReentrantLock();
		Condition slotIsAvailable = lock.newCondition();
		Condition itemIsAvaialable = lock.newCondition();
		static final int MAX = 100;

		public void produce() {
			lock.lock();
			try {
				while (queue.size() == MAX)
					waitForSignalOn(slotIsAvailable);

				String item = "item " + random.nextInt(100);
				queue.offer(item);
				System.out.println("produced " + item);
				itemIsAvaialable.signalAll();
			} finally {
				lock.unlock();
			}

		}

		public void consume() {
			lock.lock();
			try {
				while (queue.size() == 0)
					waitForSignalOn(itemIsAvaialable);

				String poll = queue.poll();
				System.out.println("consumed " + poll);
				slotIsAvailable.signalAll();
			} finally {
				lock.unlock();
			}
		}

		private void waitForSignalOn(Condition condition) {
			try {
				condition.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unused")
	private static class UsingSemaphores extends ProducerConsumer{
		private static final int MAX = 100;
		
		Semaphore full = new Semaphore(0),
				  empty= new Semaphore(MAX),
				  mutex= new Semaphore(1);
		
		public void produce() {
			decrease(empty);
			decrease(mutex);
			
			String item = ""+random.nextInt(100);
			queue.add(item);
			System.out.println("produced "+item);
			
			mutex.release();
			full.release();
		}
		
		private void decrease(Semaphore sem) {
			try {
				sem.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void consume() {
			decrease(full);
			decrease(mutex);
			
			System.out.println("consumed "+queue.poll());
			
			mutex.release();
			empty.release();
		}
	}
	
	public void produce() {
	}

	public void consume() {
	}

}
