package multithreading.basics;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.junit.Assert;

import static com.satya.common.assertions.BehavioralAssertions.*;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class OrderedExecutionOfThreads {

	public static void main(String[] args) {
		//Print T1, T2, T3, T4,...from 3 threads.
		
		new UsingSemaphores().runExample();
	}

	static class UsingSemaphores {
		Semaphore sem1 = new Semaphore(1);
		Semaphore sem2 = new Semaphore(0);
		Semaphore sem3 = new Semaphore(0);
		Semaphore sem4 = new Semaphore(0);
		
		static int countLimit = 50;
		
		List<Integer> printedIds = new ArrayList<>();
		
		class Printer implements Runnable{
			int count = 0;
			int id;
			
			public Printer(int id) {
				this.id = id;
			}
			
			public void run() {
				while(count++ < countLimit){
					if(id == 0){
						getPermitFrom(sem1, 1);
						print();
						
						addPermintTo(sem2);
						addPermintTo(sem3);
						addPermintTo(sem4);
					} else if(id == 1){
						getPermitFrom(sem2, 1);
						
						print();
						addPermintTo(sem4);
						addPermintTo(sem3);
						
					} else if(id == 2){
						getPermitFrom(sem3, 2);
						
						print();
						
						addPermintTo(sem4);
					}  else if(id == 3){
						getPermitFrom(sem4, 3);
						
						print();
						
						addPermintTo(sem1);
					}
					
				}}
			
			private void print() {
				printedIds.add(id);
			}

			private void addPermintTo(Semaphore sem) {
				sem.release();
			}

			public void getPermitFrom(Semaphore sem, int cnt){
				try {
					sem.acquire(cnt);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}}
		
		public void runExample(){
			
			Consumer<Integer> consumer = x-> {
				new Printer(x).run();
			};
			
			runConcurrently(consumer).withNumberOfThreadsEqualTo(4);
			
			System.out.println(printedIds);
			for(int i =1; i < printedIds.size(); i++){
				Assert.assertTrue(printedIds.get(i) == (printedIds.get(i-1)+1) % 4);
			}
		}}
	
	static class UsingLock {
		
	}

	public static void await(CountDownLatch latch) {
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
}
