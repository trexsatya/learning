package designpatterns.behavioral;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class CommandPatternExample {
	// Why: You will have a single command manager who does not know about how commands are gonna work.
	// CentralCommandManager e.g. RemoteControl
	// You will be able to add more commands later on, without affecting much of code.
	// You will be able to record invocations.
	// Re-factoring to patterns: Re-factor your complex switch statements or if-else ladder to command pattern.
	// You can have a callback when command is executed.
	
	// Command encapsulates all the information needed to perform some action (maybe at later time).
	// CommandManager has the job of finding the right command handler for given command.
			
	interface Command{   void execute(); 
						 default void rollback(){}; 
						 default List<Exception> rollbackFor(){ return Collections.emptyList();} }
	
	class CommandManager{
		public void handle(Command command) {}}
	
	class CommandPool{
		Stack<Command> commands = new Stack<>();
		
		void addCommand(Command command){ commands.push(command); }
		
		void executeCommands(){
			for(Command command: commands){
				try {
					command.execute();
				} catch(Exception e){
					if(command.rollbackFor().contains(e)) command.rollback();
					else throw e;}}}}
	
	public static void main(String[] args) {
		new CommandPatternExample().run(); 
	}

	@SuppressWarnings("unused")
	private void run() {
		CommandManager manager = new CommandManager();
		Command command = null;
		manager.handle(command);
		
		User user;
		CommandPool pool = new CommandPool();
//		pool.addCommand(CommandCreator.saveUser(user)); // Save user to DB
//		pool.addCommand(logTicket(user));               // Call web service.
		pool.executeCommands();
	}
	class User{}
	
}
