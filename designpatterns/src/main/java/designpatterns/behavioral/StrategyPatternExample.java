package designpatterns.behavioral;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class StrategyPatternExample {

	// The parameter based on which the algorithm implementation is selected from a given
	// family of algorithms, is not known until runtime.
	
	abstract class SortingStrategy<E>{ abstract void sort(Collection<E> data);}

	class MergeSort<E> extends SortingStrategy<E> {
		@Override
		void sort(Collection<E> data) {		}}
	
	public static void main(String[] args) {
		new StrategyPatternExample().run();
	}

	class Sorter{
		private String RUNTIME_PARAM = "FALSE";

		//@Inject
		SortingStrategy<Data> strategy;
		
		void sort(Collection<Data> data){ 
			RUNTIME_PARAM = System.getProperty("useLegacySorter");
			
			if("TRUE".equals(RUNTIME_PARAM)) strategy = new MergeSort<Data>();
			strategy.sort(data); }}
	
	private void run() {
		List<Data> data = Collections.emptyList();
		new Sorter().sort(data);
	}
	
	class Data{}
	
}
