package designpatterns.behavioral;

import java.util.Observable;
import java.util.Observer;

public class ObserverPatternExample {
	public static void main(String[] args) {
		new ObserverPatternExample().run();
	}

	class Subject extends Observable{
		void setData(Data data){ setChanged(); notifyObservers(data); }
		public void gatherData() {
			/*Fetch data and setData(data);*/ }}
	
	class ChangeReflector implements Observer{
		public void update(Observable o, Object arg) {  // Do something. 
		}}
	
	class Data{}
	
	private void run() {
		Subject subject = new Subject();
		subject.addObserver(new ChangeReflector());
		subject.gatherData();
	}
}
