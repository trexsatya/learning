package designpatterns.behavioral;

public class ChainOfResponsibilityExample {

	// When a request can be handled by multiple handlers.
	
	public static void main(String[] args) {
		new ChainOfResponsibilityExample().run();
	}	
	
	// We are not using interfaces, since if we use interface and it is modified in future; 
	// then all the implementations will need to be modified.
	
	abstract class Handler<T> { Handler<T> next; 
								abstract void handle(T req); 
								abstract void setNext(Handler<T> handler); }
	
	class ExceptionLoggerFileBased extends Handler<Exception>{
		void handle(Exception req) {
			// Log the exception in file.
			if(next != null) next.handle(req);} //Dispatch the request to next in the link.
		
		void setNext(Handler<Exception> handler) {
			this.next = handler; }}
	
	abstract class ExceptionLoggerNetworkBased extends Handler<Exception>{}
	
	private void run() {
		Handler<Exception> rootHandler = new ExceptionLoggerFileBased();
		rootHandler.handle(new Exception());
	}
}
