package designpatterns.structural;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JTextArea;

public class CompositePatternExample {

	// All component based frameworks are examples of composite pattern.
	//e.g. Swing, JSF, Struts. They all move events, actions and calculations down the hierarchy or up the hierarchy.
	
	public static void main(String[] args) {
		new CompositePatternExample().run();
	}

	private void run() {
		Container container = new JPanel(); // Container extends Component
		Component component = new JTextArea();
		container.add(component);
		
		container.getLocation(); // Iterates through parents and sums up their x and y coordinates 
								 // with nativeContainer's peer's coordinates.  
		
	}
}
