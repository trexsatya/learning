package fp.utils;

import java.util.Objects;

public interface Function<T,R> extends java.util.function.Function<T, R>{


    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    default R forInput(T t){ return apply(t); }

    /**
     * Returns a composed function that first applies the {@code before}
     * function to its input, and then applies this function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param <V> the type of input to the {@code before} java.util.function.Function, and to the
     *           composed java.util.function.Function
     * @param before the java.util.function.Function to apply before this java.util.function.Function is applied
     * @return a composed java.util.function.Function that first applies the {@code before}
     * java.util.function.Function and then applies this java.util.function.Function
     * @throws NullPointerException if before is null
     *
     * @see #andThen(java.util.function.Function)
     */
    default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }

    /**
     * Returns a composed java.util.function.Function that first applies this java.util.function.Function to
     * its input, and then applies the {@code after} java.util.function.Function to the result.
     * If evaluation of either java.util.function.Function throws an exception, it is relayed to
     * the caller of the composed java.util.function.Function.
     *
     * @param <V> the type of output of the {@code after} java.util.function.Function, and of the
     *           composed java.util.function.Function
     * @param after the java.util.function.Function to apply after this java.util.function.Function is applied
     * @return a composed java.util.function.Function that first applies this java.util.function.Function and then
     * applies the {@code after} java.util.function.Function
     * @throws NullPointerException if after is null
     *
     * @see #compose(java.util.function.Function)
     */
    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }

    /**
     * Returns a java.util.function.Function that always returns its input argument.
     *
     * @param <T> the type of the input and output objects to the java.util.function.Function
     * @return a java.util.function.Function that always returns its input argument
     */
    static <T> Function<T, T> identity() {
        return t -> t;
    }
}
