package fp.basics;

import java.util.function.Function;
import java.util.function.IntFunction;

import javaslang.Tuple;
import javaslang.Tuple2;

public class JavaSlangExamples {

	public abstract static class Node<T> {

//	    public Node(T data) { this.data = data; }

	    public abstract void setData(T data); // {
//	        System.out.println("Node.setData");
//	        this.data = data;
//	    }
	}
	
	public static class MyNode extends Node<Integer> {
//	    public MyNode(Integer data) { super(data); }

	    public void setData(Integer data) {
	        System.out.println("MyNode.setData");
//	        super.setData(data);
	    }
	}
	
	public static void main(String[] args) {
		Tuple2<String, Integer> tpl = Tuple.of("Java", 8).map( s-> s+"slang", i -> i / 2);
		System.out.println(tpl);
		
		String transformed = tpl.transform(
		        (s, i) -> s + "slang " + i / 4
		);
		System.out.println(transformed);
		
		MyNode mn = new MyNode();
		Node n = mn;            // A raw type - compiler throws an unchecked warning
		n.setData("Hello");     
//		Integer x = mn.data;    // Causes a ClassCastException to be thrown.
		
	}
	
	/**
	 * Pure Functions: First order functions
	 * Function abstractions: currying, partial application, composition
	 * Functional Data types (Monads): Functor, Monad, Optional,  
	 */
}
