package fp.basics;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.base.MoreObjects;

import static java.util.Comparator.*;
import lombok.Getter;
import static java.util.stream.Collectors.*;

public class JavaEightExamples {
	
	static class Employee {
		Long id; String name;int age;
		public Employee(long id, String name, int age) {
			this.id = id; this.age = age; this.name = name;
		}
		public Long id(){
			return id;
		}
		public String name(){
			return name;
		}
		public double age(){
			return age;
		}
		@Override
		public String toString() {
			return 
					MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("name", name)
			.add("age", age)
			.toString();
			
		}
	}
	public static void main(String[] args) {
		Employee e1 = new  Employee(1, "one", 13);
		Employee e2 = new  Employee(2, "two", 23);
		Employee e3 = new  Employee(3, "three", 16);
		Employee e4 = new  Employee(4, "four", 18);
		Employee e5 = new  Employee(5, "five", 18);
		
		List<Employee> list = Arrays.asList(e1,e2,e3,e4, e5);
		
		int groupSize = 10;
		
		IntFunction<String> findRange = x -> (x/ 10)*groupSize +"-"+ ((x/ 10)+1)*groupSize;  
		
		Map<String, List<Employee>> collect = list.stream().filter(Objects::nonNull).
			sorted (comparing(Employee::age).thenComparing(Employee::name))
			.collect ( groupingBy (e -> findRange.apply(e.age)));
		
		/**
		 * Print a report of employees grouped by age range, 
		 * each group should have the list of employees sorted by first age then name. 
		 */
		System.out.println(collect);
		
		/**
		 * Advantages of forEach:
		 *  Minor: readability, reusable lambdas
		 *  Major: parallelism
		 *  Special: no break, return; implies think of something more functional approach to do this.
		 */
		
		
		/**
		 * Map : apply a function (x -> y) to each element,
		 * Flat map:  apply a function (x -> stream(y)) to each element,
		 */
		
		int[] x = {1,2,3};
		List<Integer> out = Arrays.stream(x).
				flatMap(n -> n == 2 ? IntStream.of(n,n,n) : IntStream.of(n))
					.boxed().collect(toList());
		System.out.println(out);
		
		/**
		 * Use of optional : No null checks, no null pointer exceptions
		 * 
		 */
		class B{
			String getName(){ return "name";}
		}
		class A{
			B b;
			public B getB(){ return b;}
		}
		
		A a = new A();
		//Even though getB() returns null, there is no exception; and default value is returned.
		System.out.println(resolve(()->a.getB().getName()).orElse("Empty"));
		
		a.b = new B();
		System.out.println(resolve(()->a.getB().getName()).orElse("Empty"));
	}
	
	public static <T> Optional<T> resolve(Supplier<T> resolver) {
	    try {
	        T result = resolver.get();
	        return Optional.ofNullable(result);
	    }
	    catch (NullPointerException e) {
	        return Optional.empty();
	    }
	}
}
