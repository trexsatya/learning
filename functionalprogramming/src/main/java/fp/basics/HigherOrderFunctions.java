package fp.basics;

import static com.satya.common.assertions.BehavioralAssertions.calling;
import static com.satya.common.assertions.BehavioralAssertions.forEach;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fp.utils.Function;
import fp.utils.Memoizer;

public class HigherOrderFunctions {

	public static void main(String[] args) {
		
		int[] prime = { 3, 5};
		forEach(prime, p -> {
			calling(PrimeNumberTester.isPrime(p)).shouldReturn(true);});
		
	}
	
	static class PrimeNumberTester{
		
    	static Function<Integer, Set<Integer>> factorsFn = n -> {
    		  return IntStream.rangeClosed(1, (int)Math.sqrt(n))
    				  .filter( it -> isFactor(it, n))
    				  .flatMap(it -> IntStream.of(it, n / it)).boxed().collect(Collectors.toSet());
    	};
    	
    	static Function<Integer,Set<Integer>> getFactors = Memoizer.memoize(factorsFn);
    	
		public static boolean isPrime(int number) {
			return number == 2 || sumFactors(number) == number + 1;
		}

		static long sumFactors(int n){ return 
				getFactors.apply(n).stream()
							.reduce(0, (a,b)-> a+b); }
		
		static boolean isFactor(int it, Integer n){ return n % it == 0;}
		
	}

}
