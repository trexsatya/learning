package com.satya.common.assertions;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.OptionalInt;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.function.LongConsumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.apache.commons.beanutils.PropertyUtils;

import com.google.common.primitives.Ints;
import com.satya.common.assertions.BehavioralAssertions.ConcurrentRunner;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static com.satya.common.assertions.BehavioralAssertions.await;
import static com.satya.common.assertions.BehavioralAssertions.loopOver;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Every.everyItem;

public class BehavioralAssertions {

	static {
		turnAssertionsOn();
	}

	// callTo(x.isPrime(3)).shouldReturn(false);

	public static void await(CountDownLatch latch){
		try {
			latch.await(50, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static int withNumberOfThreadsEqualTo(int x){
		return x;
	}
	
	public static class ConcurrentRunner{
		@NonNull Consumer<Integer> consumer;
		int N;
		public ConcurrentRunner(Consumer<Integer> consumer) {
			this.consumer = consumer;
		}
		
		public  void withNumberOfThreadsEqualTo(int N){
			CountDownLatch latch = new CountDownLatch(N);
			
			loopOver(0, N, i->{
				new Thread(()-> {
					consumer.accept((int) i);
					latch.countDown();
				}).start();
			});
			
			await(latch);
		}
		
	}
	
	public static  ConcurrentRunner runConcurrently(Consumer<Integer> consumer){
		return new ConcurrentRunner(consumer);
	}
	
	public static List<?> EMPTY_LIST = Collections.emptyList();

	public static class MethodCallResultWrapper<T> {
		private T value;

		public MethodCallResultWrapper(T t) {
			this.value = t;
		}

		public MethodCallResultWrapper<T> shouldReturn(T t) {
			assert nullSafeEquals(t, value) : "Expected <" + t + "> but was actually <" + value + ">";
			return this;
		}

		public MethodCallResultWrapper<T> shouldReturn(Predicate<T> cond) {
			assert cond.test(value) : "Condition not satisfied by actual value <" + value + ">";
			return this;
		}

		public MethodCallResultWrapper<T> shouldReturn(Matcher<T> t) {
			assert t.condition.matches(value) : "Expected <" + t + "> but was actually <" + value + ">";
			return this;
		}
		
		public MethodCallResultWrapper<T> and(Matcher<T> t){
			assert t.condition.matches(value) : "Expected <" + t + "> but was actually <" + value + ">";
			return this;
		}
		
		public MethodCallResultWrapper<T> and(Predicate<T> cond) {
			assert cond.test(value) : "Condition not satisfied by actual value <" + value + ">";
			return this;
		}
	}

	public static interface MatcherCondition<T> {
		boolean matches(T value);
	}

	public static class Matcher<T> {
		String description;
		MatcherCondition<T> condition;

		public Matcher(String desc, MatcherCondition<T> cond) {
			condition = cond;
			description = desc;
		}

		@Override
		public String toString() {
			return description;
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> Matcher<T> greaterThan(T val) {
		return new Matcher<T>("a value greater than " + val, value -> ((Comparable<T>) value).compareTo(val) > 0);
	}

	@SuppressWarnings("unchecked")
	public static <T> Matcher<T> lessThan(T val) {
		return new Matcher<T>("a value less than " + val, value -> ((Comparable<T>) value).compareTo(val) < 0);
	}

	@SuppressWarnings("rawtypes")
	public static <T> Matcher<T> anEmptyList() {
		return new Matcher<T>("an empty list", value -> ((List) value).isEmpty());
	}

	@SuppressWarnings("rawtypes")
	public static <T> Matcher<T> listWithSize(int n) {
		return new Matcher<T>("a list having " + n + " element" + (n > 1 ? "s" : ""),
				value -> ((List) value).size() == n);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> Matcher<List<T>> listWithItems(OrderingCondition<T> ordering, T... items) {
		return new Matcher("a list having " + Arrays.asList(items), value -> ordering.matches((List) value, items));
	}
	
//	@SuppressWarnings({ "unchecked" })
//	public static <T> Matcher<List<T>> listWithItems(T... items) {
//		return listWithItems(inAnyOrder(), items);
//	}

	public static <T> Matcher<T> anObjectOfType(Class<?> cls) {
		return new Matcher<T>("an object of type " + cls, value -> value.getClass().equals(cls));
	}

	public static interface OrderingCondition<T> {
		@SuppressWarnings("unchecked")
		boolean matches(List<T> list, T... items);
	}

	public static <T> OrderingCondition<T> inOrder() {
		return (list, its) -> {
			T[] items = its;
			OptionalInt breaksOrder = IntStream.range(0, items.length)
					.filter(indx -> nullSafeEquals(list.get(indx), items[indx])).findFirst();
			return breaksOrder.isPresent();
		};
	}

	public static <T> OrderingCondition<T> inAnyOrder() {
		return (list, its) -> {
			T[] items = its;
			return list.containsAll(Arrays.asList(items));
		};
	}

	public static <T> MethodCallResultWrapper<T> calling(T t) {
		return new MethodCallResultWrapper<T>(t);
	}

	private static void turnAssertionsOn() {
		Class<?> cls = BehavioralAssertions.class;
		cls.getClassLoader().setClassAssertionStatus(cls.getName(), true);
	}

	public static void forEach(int[] args, Consumer<Integer> consumer) {
		Arrays.stream(args).forEach(it -> consumer.accept(it));
	}

	public static <T> AnyOfAssertor<T> anyOf(Iterable<T> iterable) {
		return new AnyOfAssertor<T>(iterable);
	}

	public static <T> AnyOfAssertor<T> anyOf(T[] array) {
		return new AnyOfAssertor<T>(Arrays.asList(array));
	}

	public static class AnyOfAssertor<T> {
		Iterable<T> iterable;

		public AnyOfAssertor(Iterable<T> iterable) {
			this.iterable = iterable;
		}

		public void satisfiesCondition(Predicate<? super T> predicate) {
			Stream<T> stream = StreamSupport.stream(iterable.spliterator(), false);
			if (!stream.anyMatch(predicate))
				assert false : " no element from " + iterable + " satisfied the given condition " + predicate;
		}

		public void shouldSatisfyCondition(Predicate<? super T> predicate) {
			satisfiesCondition(predicate);
		}

	}

	public static void loopOver(int x, int y, IntConsumer consumer){
		IntStream.range(x, y).forEach(consumer);
	}
	
	public static void loopUntil(int x, IntConsumer consumer){
		IntStream.range(0, x).forEach(consumer);
	}
	
	public static void loopUntilLong(long  x, LongConsumer consumer){
		LongStream.range(0, x).forEach(consumer);
	}
	
	public static void loopOverLong(long  x,long y, LongConsumer consumer){
		LongStream.range( x, y).forEach(consumer);
	}
	
	public static IntSummaryStatistics statistics(int[] ar){
		return Arrays.stream(ar).summaryStatistics();
	}
	
	public static <T> ObjectIntrospector<T> given(T o) {
		return new ObjectIntrospector<T>(o);
	}

	public static class ObjectIntrospector<T> {
		T target;

		public ObjectIntrospector(T o) {
			this.target = o;
		}

		public PropertyAssertor itsProperty(String propertyName) {
			Object value = null;
			Class<?> type = null;
			try {
				value = PropertyUtils.getProperty(target, propertyName);
				type = value.getClass();
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
				e.printStackTrace();
			}

			return new PropertyAssertor(propertyName, type, value);
		}

		public class PropertyAssertor {
			Object value;
			String name;
			Class<?> type;

			public PropertyAssertor(String name, Class<?> type, Object value) {
				this.name = name;
				this.value = value;
				this.type = type;
			}

			public void shouldBeNull() {
				assert value == null : name + " was expected to be <null> but is actually <" + value + ">";
			}

			public void shouldBe(Object val) {
				assert nullSafeEquals(value, val) : name + " was expected to be <" + val + "> but was actually <"
						+ value + ">";
			}

			public void shouldContain(Object o) {

			}

			public void onIteration(Consumer<Object> consumer) {
				iterable().forEach(consumer);
			}

			public Stream<Object> iterable() {
				Object[] objs = (Object[]) value;
				return Arrays.stream(objs);
			}

		}
	}

	private static boolean nullSafeEquals(Object a, Object b) {
		if (a == b)
			return true;
		if (a == null)
			return b == null ? true : b.equals(a);
		if (b == null)
			return a == null ? true : a.equals(b);
		return a.equals(b);
	}
	
	public static <T extends Comparable<T>> T min(T...vals){
		T min = vals[0];
		for(int i=0; i < vals.length; i++){
			if(vals[i].compareTo(min) < 0) min = vals[i];
		}
		return min;
	}
	
	public static <T extends Comparable<T>> T max(T...vals){
		T max = vals[0];
		for(int i=0; i < vals.length; i++){
			if(vals[i].compareTo(max) > 0) max = vals[i];
		}
		return max;
	}
	
	public static void main(String[] args) {
		System.out.println(max(145,23,5,2));
	}
}
