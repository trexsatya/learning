package com.satya.collections.cache;

import static java.util.Arrays.*;

import java.util.LinkedHashMap;
import java.util.Objects;

import static com.satya.common.assertions.BehavioralAssertions.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.cache.Cache;
import com.satya.common.assertions.BehavioralAssertions;

public class LRUCacheBehavior {
	
	Cache<Integer,String> cache; 
	
	@Before
	public void beforeEveryTest(){ }
	
	@Test
	public void should_act_as_a_Map(){
		
		
	}
	
	@Test
	public void should_expire_least_recently_accessed() {
		
		
		/**
		 * Choices: Weak keys, Weak/Soft values (If reference is weak it surely will be garbage collected in next cycle,
		 * 				while a soft reference sticks around while the memory is plentiful enough.
		 * Cache loading computations
		 * Lock segment for write
		 * Lock segment before and after loading computation
		 * Eviction algorithms: Evict only when size limit of cache is reached
		 * 						Evict when size limit is approaching, evict entries which have not been used 
		 * 						  	(recently, more often)
		 * 						Remove entry automatically if specified time expired from time of creation or most recent replacement.
		 * 						
		 * 
		 */
	}
}
