package com.satya.collections.cache;

import java.util.HashMap;


public class LRUCache<K,V> {

	static class Node<K,V> {
	    K key;
	    V value;
	    Node<K,V> pre, next;
	 
	    public Node(K key, V value){
	        this.key = key;
	        this.value = value;
	    }
	}

	int capacity;
	HashMap<K,Node<K,V>> map = new HashMap<K, Node<K,V>>();
	private Node<K, V> mostRecent;
	private Node<K, V> leastRecent;
	
	public LRUCache(int cap) {
		this.capacity = cap;
	}
	
	public V get(K key){
		if(map.containsKey(key)){
            Node<K,V> n = map.get(key);
            remove(n);
            setMostRecent(n);
            return n.value;
        }
 
        return null;
	}

	private void setMostRecent(Node<K, V> n) {
		n.next = mostRecent;
		n.pre = null;
		
		if(mostRecent != null) mostRecent.pre = n;
		//update head to point to new head
		mostRecent = n;
		
		//used while first addition
		if(leastRecent == null) leastRecent = mostRecent;
		
	}

	private void connect(Node<K, V> pre, Node<K, V> next) {
		pre.next = next;
		next.pre = pre;
	}

	private void remove(Node<K, V> n) {
		connect(n.pre, n.next);	
		if(n.pre == null) mostRecent = n.next;
		if(n.next == null) leastRecent = n.pre;
	}
	
	public void set(K key, V value){
		if(map.containsKey(key)){
			Node<K, V> old = map.get(key);
			old.value = value;
			moveToMostRecent(old);
		} else {
			Node<K,V> newNode = new Node<K, V>(key, value);
			if(map.size() >= capacity){
				map.remove(leastRecent.key);
				remove(leastRecent);
			}  
			setMostRecent(newNode);
			map.put(key, newNode);
		}
	}

	
	private void moveToMostRecent(Node<K, V> old) {
		remove(old);
	}

}
