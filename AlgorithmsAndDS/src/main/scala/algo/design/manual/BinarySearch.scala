package algo.design.manual

object BinarySearch {
  /**
   * One sided Binary Search: Suppose you are supplied with zeroes followed by unbounded ones.
   * Find the position of transition from 0 to 1 in sequence.
   * Move in steps of 2^n, when there is a 1, now we have bounded array, apply Binary search.
   * 
   */
}