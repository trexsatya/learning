package algo.design.manual
import myscala.enriched.collections._

object Sorting {

  def main(args: Array[String]): Unit = {
    var ar = Array(1, 12, 4, 11, 7, 2, 3, 1, 16)

    selectionSort(ar)
    ar = Array(1, 12, 4, 11, 7, 2, 3, 1, 16)
    heapSort(ar)
    ar = Array(1, 12, 4, 11, 7, 2, 3, 1, 16)
    insertionSort(ar)
    ar = Array(1, 12, 4, 11, 7, 2, 3, 1, 16)
    quickSort(ar).dump()

    println(msort((a: Int, b: Int) => a < b)(ar.toList))
  }

  def selectionSort(ar: Array[Int]) = {
    /**
     *  Finds the minimum from (sub)array in O(n) time
     */
    val findMin = (ar: Array[Int], startingFromHere: Int) => {
      var mini = startingFromHere

      mini + 1 to ar.length - 1 foreach (i =>
        if (ar(i) < ar(mini)) mini = i)
      mini
    }
    /**
     * Deletes an element at given index from array in O(1) time. Since array is unsorted we can just swap the
     * last element with the element to be deleted. Here the index of that 'last' element is given.
     */
    val deleteFromArray = (ar: Array[Int], deleteHere: Int, swapHere: Int) => {
      val tmp = ar(deleteHere);
      ar(deleteHere) = ar(swapHere)
      ar(swapHere) = tmp;
    }

    0 to ar.length - 1 foreach { i =>
      deleteFromArray(ar, findMin(ar, i), i)
    }
    ar.dump()
  }

  /**
   * Now we can improve the performance of these two operations by using HEAP data structure
   * Find minimum in O(1)
   * Delete given key in O(logN)
   */
  def heapSort(ar: Array[Int]) = {
    val parent: Int => Int = x => if (x == 0) -1 else x / 2
    var size = ar.length

    def insertIntoHeap(x: Int) = {}

    def swap(a: Int, b: Int) = { var tmp = ar(a); ar(a) = ar(b); ar(b) = tmp }

    def bubbleUp(x: Int): Unit = {
      if (parent(x) == -1) return
      if (ar(x) < ar(parent(x))) {
        swap(x, parent(x))
        bubbleUp(parent(x))
      }
    }
    /**
     * Costs O(NlogN) time
     */
    var buildHeap = () => {
      0 to ar.length - 1 foreach bubbleUp
    }

    val children = (x: Int) => Array(2 * x, 2 * x + 1)

    def extractMin(): Int = {
      var min = ar(0)
      ar(0) = ar(size - 1)
      size -= 1
      bubbleDown(0)
      min
    }

    def bubbleDown(x: Int): Unit = {
      var mini = x
      children(x) filter (_ < size) foreach { c =>
        if (ar(c) < ar(mini)) mini = c
      }
      if (mini != x) {
        swap(x, mini)
        bubbleDown(mini)
      }
    }

    /**
     * Building heap by bubble down costs O(N) time.
     * In a full binary tree on n nodes, there are
     * n/2 nodes at height 0
     * n/4 nodes at height 1
     * ..
     * n/2^ h+1 nodes at height h
     * sum (over h=0 to lg(n)) { (number of nodes at height h) * h}
     * which is <= n * sum (over h=0 to lg(n)) { h/2^h }
     * which is <= 2n
     * T(n) = 2T(n/2) + O(lg n)
     */
    buildHeap = () => {
      ar.length - 1 to 0 by -1 foreach bubbleDown
    }

    buildHeap()
    val sorted = ar map (x => extractMin())
    sorted.dump()
  }

  def insertionSort(ar: Array[Int]) = {
    def swap(a: Int, b: Int) = { var tmp = ar(a); ar(a) = ar(b); ar(b) = tmp }

    /**
     * Insert into (sub)array. This takes O(N) time.
     * Using Balanced Binary Search Tree improves this to O(lgN)
     */
    def insert(x: Int, startSearchingHere: Int) = {
      var i = startSearchingHere
      val tmp = ar(x)
      while (i >= 0 && ar(i) >= tmp) {
        swap(i, i + 1)
        i -= 1
      }
    }

    1 to ar.length - 1 foreach { i =>
      //       ar.dump()
      insert(i, i - 1)
    }
    ar.dump()
  }

  /**
   * Divide and conquer: O(NlgN)
   */
  def quickSort(ar: Array[Int]): Array[Int] = {
    if (ar.length <= 1) ar
    else {
      val pivot = ar(ar.length / 2) //middle element
      Array.concat(quickSort(ar filter (pivot > _)),
        ar filter (pivot == _),
        quickSort(ar filter (pivot < _)))
    }
  }

  /**
   * Divide and conquer: O(NlgN). To avoid stack overflow, use stream (non-strict collection).
   * 
   */
  def msort[T](less: (T, T) => Boolean)(xs: List[T]): List[T] = {
    def merge(left: List[T], right: List[T]): Stream[T] = (left, right) match {
      case (x :: xs, y :: ys) if less(x, y) => Stream.cons(x, merge(xs, right))
      case (x :: xs, y :: ys)               => Stream.cons(y, merge(left, ys))
      case _                                => if (left.isEmpty) right.toStream else left.toStream
    }
    val n = xs.length / 2
    if (n == 0) xs
    else {
      val (ys, zs) = xs splitAt n
      merge(msort(less)(ys), msort(less)(zs)).toList
    }
  }
}