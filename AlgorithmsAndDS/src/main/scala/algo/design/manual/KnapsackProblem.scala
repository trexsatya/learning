package algo.design.manual

object KnapsackProblem {
  /**
   * Given: S = {s1,s2,...,sn} and a target number T
   * Give: A subset of S, such that it's sum is T.
   * 
   * S = {1,2,5,9,10}
   * T = 22
   * Wrong Ideas (Greedy): First-Fit left to right; Best-Fit smaller to larger; larger to smaller;
   *   
   */
}