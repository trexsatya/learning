package algo.design.manual

object WaringsProblem {
  /**
   * Can every integer be represented as four or less integer-squares.
   * e.g 78 = 8^2 + 3^2 + 2^2 + 1^2 = 7^2 + 5^2 + 2^2
   */
  
  /**
   * PYRAMIDAL NUMBER: e.g 1, 4, 10, 20, 35, 56, 84, 120
   * Can every integer be represented as five or less pyramidal-numbers.
   * Pyramidal number is a number which can be represented in form: (m^3-m)/6
   * 
   * Build table[n] for pyramidal numbers. O(n ^ 1/3)
   * For each k => smallest set of pyramidal numbers that add up to exactly k. => Knapsack Problem
   * Build sorted all-pair-sums[] of pyramidal numbers.
   * Check whether k is in pyramidal numbers table.
   * If not check whether (k- p[i]) is in all-pair-sums[]. Use Binary Search.
   * If not check whether (k- all-pair-sums[i]) is in all-pair-sums[]. Use Binary Search.
   *   
   * Use hash table/ bit vector instead of sorted array.
   * Use interpolation-search instead of binary-search.
   * 
   * Avoid sum-of-four computation on any k if k-1 was sum-of-three, because 1 is a pyramidal number.
   *    
   */
}