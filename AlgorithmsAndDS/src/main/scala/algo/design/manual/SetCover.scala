package algo.design.manual

object SetCover {
  /**
   * Given: A set of sets -> S1 = {1,3,5}, S2 = {2,4}, S3 = {1,4}, and S4 = {2,5}
   * Give: Which of these create a universal set ({1,2,3,4,5}) on union.
   * 
   * Wrong Ideas: Best-Fit Largest first;
   * 
   */
}