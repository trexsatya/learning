package topcoder;

import java.util.Arrays;

import static com.satya.utils.Utils.*;

import codecheff.problems.common.TestCasesReader;
import codecheff.problems.common.TestCasesReader.TestCaseDataSupplier;

public class XorOrderDiv1 {
    static final long MODULO = (long) (1e9 + 7);
 
    public static void main(String[] args) {
    	TestCaseDataSupplier data = TestCasesReader.reader();
    	
    	int res = new XorOrderDiv1().get(3,5,1,3);
    	System.out.println(res);
    	
	}
    
    public int get(int m, int nrOfcontestants, int a0, int b) {
    	//Number of days = 2^m => each day will have m bits
    	
        int[] a = new int[nrOfcontestants];
        
        //skills
        for (int i = 0; i < nrOfcontestants; ++i) a[i] = (int) ((a0 + i * (long) b) & ((1 << m) - 1));
        Arrays.sort(a);
        
        print(a);

        for(int ai:a) print(ai^0); //finishing times on given day a(i)^j
        
        int[] sum = new int[nrOfcontestants];
        long[] exp = new long[nrOfcontestants];
        
        for (int bit = m - 1; bit >= 0; --bit) {
            for (int i = 0; i < nrOfcontestants; ) {
                int j = i;
                while (j < nrOfcontestants && (a[j] >> (bit + 1)) == (a[i] >> (bit + 1))) ++j;
                //for any contestant, go through all remaining contestants;
                
                int k = i;
                while (k < nrOfcontestants && (a[k] >> bit) == (a[i] >> bit)) ++k;
                
                int first = k - i;
                int second = j - k;
                if (second > 0) {
                    for (int t = i; t < k; ++t) {
                        sum[t] += second;
                        exp[t] += second * (long) sum[t];
                        exp[t] %= MODULO;
                    }
                }
                if (first > 0) {
                    for (int t = k; t < j; ++t) {
                        sum[t] += first;
                        exp[t] += first * (long) sum[t];
                        exp[t] %= MODULO;
                    }
                }
                i = j;
            }
        }
        long mult = 1L << (m - 1);
        mult %= MODULO;
        long res = 0;
        for (long x : exp) {
            res ^= x * mult % MODULO;
        }
        return (int) res;
    }
 
}