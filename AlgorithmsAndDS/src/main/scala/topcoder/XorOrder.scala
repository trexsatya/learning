package topcoder
import myscala.enriched.collections._
import scala.collection.mutable.ArrayBuffer
import java.util.Arrays

object XorOrder {

  def main(args: Array[String]): Unit = {
    println("")
    val (m,contestants,a0, b) = (readInt, readInt, readInt, readInt)
    val days = 1 << m //=> m bits
    val n =  contestants

    val MOD = (Math.pow(10,9).intValue() + 7)
    
    var skills = array( i => a0+i*b % (1<<m), 0 to contestants-1)
//    skills.dump()
    
    val a = skills
    
    var timeTaken = array(i=> 0, 0 to a.length-1)
    
    var penalty = array(i=> 0, 0 to timeTaken.length-1)
    
    0 to days-1 foreach{j=>
      0 to a.length-1 foreach {i=>
        println(s"day: $j contestant: $i")
        
        println(a(i).toBinaryString)
        println(j.toBinaryString)
        
        timeTaken(i) = a(i)^j 
        println(timeTaken(i).toBinaryString)
        
      }
      
      val winner = timeTaken.sorted
      
      0 to timeTaken.length-1 foreach {t=>
        val x = Arrays.binarySearch(winner.toArray, timeTaken(t))% MOD
        penalty(t) += (x*x) 
      }
      
      0 to penalty.length-1 foreach{i=>
        println(s"day: $j contestant: $i penalty: ${penalty(i).toBinaryString}")
      }
    }
    
    println(penalty.foldLeft(0l)((r,a) =>r^a))
    
  }
}