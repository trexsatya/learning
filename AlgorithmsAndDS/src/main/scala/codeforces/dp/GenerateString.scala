package codeforces.dp
import myscala.enriched.collections._
import scala.collection.mutable.Buffer

object GenerateString {
  
  /**
   * 10 3
     cbaaccaccb
   * 1  0  0  0  1  1  0  1  1  0  
   * Find max number of ones after flipping at most 3 zeroes.
   */
  def main(args: Array[String]): Unit = {
    
    for(t <- 0 until readInt){
      var Array(n,k) = readLine.split(" ").map(_.toInt)
      var str = readLine
      
      solve(k,str)
    }
    
  }
  
  
  def solve(k: Int, str: String) = {
//    f(0, 'a') -> f(1 , 'a')
    
    var max = 0l
    for(char <- 'a' to 'z'){
       var ar = str map({ 
          case `char` => 1
          case x => 0
        }) toArray
       
        isDebug = if(char == 'c') true else false 
        
       max = math.max(solveMaxOnes(ar, k), max)
    }
    
    println(max)
  }
  
  def solveMaxOnes(ar:Array[Int], K:Int): Long = {
//    var ar = Array(0,0,1,0,1,0,0,1,0,1,0,1,1,1)
    //Max number of ones after flipping at most k zeroes.
//    val K = 3
    
    //After stripping zeroes from left
    //Length of Maximum countOne sub-array with at most k zeroes + (k-number of zeroes there)

 
     
    return 0
  }
  
  var isDebug = false
  var debug = (x:Any) =>{
    if(isDebug) println(x)
  }
  
}