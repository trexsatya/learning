package functional.algos

object Lists {

  def main(args: Array[String]): Unit = {
    var list = List(1, 2, 3)
    println(s"${prefixes(list)}")
    println(s"${suffixes(list)}")

    println(s"${reverse(list)}")

    println("permutations:")
    xvariations(list, list.length) foreach println

    println("combinations:")
    xcombinations(list, list.length - 1) foreach println

    println("subsets:")
    xsubsets(list) foreach println

    println("LIS:")
    println(longestIncreasingSubsequence(List(3, 1, 5, 4, 6, 1, 9), Ordering.by { n: Int => n }))

    println("LCSum:")
    println(largestSumOfContiguousSubList(List(3, 4, -5, 7, 8, 0, -2))) // You don't need to pass 'implicit' parameter.

    println("Maximum sum increasing subsequence:")
    println(maximumSumIncreasingSubsequence(List(1, 101, 2, 3, 100, 4, 5)))
    
    val toInt = (x:Char) => (x).toInt
    
    list = "BBABCBCAB" map toInt toList
    
    
    println("longest palindromic subsequence:")
    println(list)
    println(longestPalindromicSubsequence(list))
    
  }

  def prefixes[A](list: List[A]): List[List[A]] = {
    def helper(acc: List[List[A]], r: List[A]): List[List[A]] = {
      if (r.isEmpty) acc
      else helper(List(acc.head ::: List(r.head)) ::: acc, r.tail)
    }
    if (list.isEmpty) List()
    else helper(List(List(list.head)), list.tail)
  }

  def suffixes[A](list: List[A]): List[List[A]] =
    if (list.isEmpty) List()
    else suffixes(list.tail) ::: List(list)

  def reverse[A](list: List[A]): List[A] = {
    def loop(s: List[A], d: List[A]): List[A] =
      if (s.isEmpty) d
      else loop(s.tail, d.:::(List(s.head)))

    loop(list, List.empty)
  }

  /**
   * k-permutation
   */

  type V[A] = List[A]

  def xvariations[A](list: V[A], k: Int): V[V[A]] = {
    def mixmany(x: A, ll: V[V[A]]): V[V[A]] = ll match {
      case head :: tail => foldone(x, head).:::(mixmany(x, tail))
      case _            => Nil
    }

    def foldone(x: A, ll: V[A]): V[V[A]] =
      (1 to ll.length).foldLeft(List(x :: ll))((a, i) => (mixone(i, x, ll)) :: a)

    def mixone(i: Int, x: A, ll: V[A]): V[A] =
      ll.slice(0, i) ::: (x :: ll.slice(i, ll.length))

    if (k > list.size) Nil
    else list match {
      case _ :: _ if k == 1 => list.map(List(_))
      case head :: tail     => mixmany(head, xvariations(tail, k - 1)) ::: xvariations(tail, k)
      case _                => Nil
    }
  }

  /**
   * k-combination
   */
  def xcombinations[A](list: List[A], k: Int): List[List[A]] = {
    if (k > list.size) Nil
    else list match {
      case _ :: _ if k == 1 =>
        list.map(List(_))
      case hd :: tl =>
        xcombinations(tl, k - 1).map(hd :: _) ::: xcombinations(tl, k)
      case _ => Nil
    }
  }

  def xsubsets[A](list: List[A]): List[List[A]] =
    (2 to list.length).foldLeft(xcombinations(list, 1))((a, i) => xcombinations(list, i) ::: a)

  /**
   * To avoid stack overflow for large lists, use tail recursion in merge function.
   */
  def msort[T](less: (T, T) => Boolean)(xs: List[T]): List[T] = {
    def merge(xs: List[T], ys: List[T], acc: List[T]): List[T] =
      (xs, ys) match {
        case (Nil, _) => ys.reverse ::: acc
        case (_, Nil) => xs.reverse ::: acc
        case (x :: xs1, y :: ys1) =>
          if (less(x, y)) merge(xs1, ys, x :: acc)
          else merge(xs, ys1, y :: acc)
      }
    val n = xs.length / 2
    if (n == 0) xs
    else {
      val (ys, zs) = xs splitAt n
      merge(msort(less)(ys), msort(less)(zs), Nil).reverse
    }
  }

  def longestIncreasingSubsequence[A, B >: A](ll: V[A], ordering: Ordering[B]): List[B] = {
    // We can use the following instead:
    // zipWithIndex.map(t => (t._2, List(t._1))).toMap
    // http://stackoverflow.com/questions/17828431/convert-scalas-list-into-map-with-indicies-as-keys
    def init(i: Int, l: List[A], m: Map[Int, List[A]]): Map[Int, List[A]] =
      if (l.isEmpty) m
      else init(i + 1, l.tail, m + (i -> List(l.head)))

    def loop(i: Int, l: List[A], m: Map[Int, List[A]]): List[A] =
      if (l.isEmpty) m.maxBy(_._2.length)._2.reverse
      else {
        val f = m.filter(p => p._1 < i && ordering.lt(p._2.head, l.head))
        if (f.isEmpty) loop(i + 1, l.tail, m)
        else {
          val (_, ll) = f.maxBy(_._2.length)
          loop(i + 1, l.tail, m + (i -> ll.+:(l.head)))
        }
      }

    if (ll.isEmpty) List.empty
    else loop(1, ll.tail, init(0, ll, Map[Int, List[A]]()))
  }

  /**
   * Count the largest sum of contiguous sub list.
   *
   * http://www.geeksforgeeks.org/largest-sum-contiguous-subarray/
   *
   * NOTES: It uses the DP-approach based on Kadane’s algorithm.
   *
   * Time - O(n)
   * Space - O(n)
   */
  def largestSumOfContiguousSubList[A, B >: A](ll: V[A])(implicit num: Numeric[B]): B = {
    def loop(sm: B, gm: B, l: List[B]): B =
      if (l.isEmpty) gm
      else {
        val nsm = num.max(l.head, num.plus(sm, l.head))
        loop(nsm, num.max(gm, nsm), l.tail)
      }

    if (ll.isEmpty) fail("An empty list.")
    else loop(ll.head, ll.head, ll.tail)
  }

  /**
   * Builds the increasing sub-sequence with maximum sum.
   *
   * http://www.geeksforgeeks.org/dynamic-programming-set-14-maximum-sum-increasing-subsequence/
   *
   * Time - O(n^2)
   * Space - O(n^2)
   */
  type VI = List[Int]
  type MI = List[VI]

  def maximumSumIncreasingSubsequence(ll: VI): VI = {
    def sum(l: VI): Int = {
      l.foldLeft(0)((a, b) => a + b)
    }
    def update(l: MI, i: Int, o: VI): MI = {
      def updateHelper(h: MI, t: MI, j: Int): MI = {
        if (t.isEmpty) reverse(h)
        else if (i == j) updateHelper(o :: h, t.tail, j + 1)
        else updateHelper(t.head :: h, t.tail, j + 1)
      }
      def reverse(ll: MI): MI = {
        def reverseHelper(h: MI, t: MI): MI = {
          if (t.isEmpty) h
          else reverseHelper(t.head :: h, t.tail)
        }
        reverseHelper(List.empty[VI], ll)
      }
      updateHelper(Nil, l, 0)
    }

    def loop(msis: MI, i: Int, j: Int): MI = {
      if (i >= msis.length) msis
      else if (j >= i) loop(msis, i + 1, 0)
      else if (ll(i) > ll(j) && sum(msis(i)) < sum(msis(j)) + ll(i) && i - j == msis(i).length) {
        loop(update(msis, i, msis(j) ++ List(ll(i))), i, j + 1)
      } else loop(msis, i, j + 1)
    }

    def maxBySum(msis: MI): VI = {
      def maxBySumHelper(b: VI, bSum: Int, t: MI): VI = {
        if (t.isEmpty) b
        else if (sum(t.head) > bSum) maxBySumHelper(t.head, sum(t.head), t.tail)
        else maxBySumHelper(b, bSum, t.tail)
      }
      maxBySumHelper(msis.head, sum(msis.head), msis.tail)
    }
    maxBySum(loop(ll.map(x => List[Int](x)), 0, 0))
  }

  /**
   * Returns the longest palindromic sub-sequence of this list.
   *
   * http://www.geeksforgeeks.org/dynamic-programming-set-12-longest-palindromic-subsequence/
   *
   * Time - O(n^2)
   * Space - O(n^2)
   */
  def longestPalindromicSubsequence(ll:VI): Int = {
    def max(a: Int, b: Int) = if (a > b) a else b
    def setM(m: Map[(Int, Int), Int], cl: Int, i: Int, j: Int): Map[(Int, Int), Int] = {
      if (ll(i) == ll(j) && cl == 2) m + ((i, j) -> 2)
      else if (ll(i) == ll(j) && m((i + 1, j - 1)) == j - i - 1) m + ((i, j) -> (m((i + 1, j - 1)) + 2))
      else m + ((i, j) -> max(m((i + 1, j)), m((i, j - 1))))
    }
    def loop(m: Map[(Int, Int), Int], cl: Int, i: Int): Int = {
      if (cl > ll.length) m((0, ll.length - 1))
      else if (i >= ll.length - cl + 1) loop(m, cl + 1, 0)
      else loop(setM(m, cl, i, i + cl - 1), cl, i + 1)
    }
    def initialize(m: Map[(Int, Int), Int], i: Int, j: Int): Map[(Int, Int), Int] = {
      if (i >= ll.length) m
      else if (j >= ll.length) initialize(m, i + 1, 0)
      else if (i == j) initialize(m + ((i, j) -> 1), i, j + 1)
      else initialize(m + ((i, j) -> 0), i, j + 1)
    }
    loop(initialize(Map.empty[(Int, Int), Int], 0, 0), 2, 0)
  }

  def fail(m: String) = throw new NoSuchElementException(m)
}