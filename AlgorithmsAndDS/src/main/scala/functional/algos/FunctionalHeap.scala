package functional.algos

abstract sealed class PersistentHeap[+A <% Ordered[A]] {

  /**
   * Minimum of this HeapF.
   */
  def min: A

  /**
   * The left child of this HeapF.
   */
  def left: PersistentHeap[A]

  /**
   * The right child of this HeapF.
   */
  def right: PersistentHeap[A]

  /**
   * The size of this HeapF.
   */
  def size: Int

  /**
   * The height of this tree
   */
  def height: Int

  /**
   * Checks whether this HeapF empty or not.
   */
  def isEmpty: Boolean

  /**
   * Inserts given element 'x' into this HeapF.
   *
   * Time - O(log n)
   * Space - O(log n)
   */
  def insert[B >: A <% Ordered[B]](x: B): PersistentHeap[B] =
    if (isEmpty) PersistentHeap.make(x)
    else if (left.size < math.pow(2, left.height) - 1) 
      PersistentHeap.bubbleUp(min, left.insert(x), right)
    else if (right.size < math.pow(2, right.height) - 1) 
      PersistentHeap.bubbleUp(min, left, right.insert(x))
    else if (right.height < left.height) 
      PersistentHeap.bubbleUp(min, left, right.insert(x))
    else PersistentHeap.bubbleUp(min, left.insert(x), right)

  /**
   * Removes minimum element from this HeapF.
   *
   * Time - O(log n)
   * Space - O(log n)
   */
  def remove: PersistentHeap[A] = {
    def floatLeft[A <% Ordered[A]](x: A, l: PersistentHeap[A], r: PersistentHeap[A]): PersistentHeap[A] = l match {
      case Branch(y, lt, rt, _, _) => PersistentHeap.make(y, PersistentHeap.make(x, lt, rt), r)
      case _ => PersistentHeap.make(x, l, r)
    }

    def floatRight[A <% Ordered[A]](x: A, l: PersistentHeap[A], r: PersistentHeap[A]): PersistentHeap[A] = r match {
      case Branch(y, lt, rt, _, _) => PersistentHeap.make(y, l, PersistentHeap.make(x, lt, rt))
      case _ => PersistentHeap.make(x, l, r)
    }

    def mergeChildren(l: PersistentHeap[A], r: PersistentHeap[A]): PersistentHeap[A] = 
      if (l.isEmpty && r.isEmpty) PersistentHeap.empty
      else if (l.size < math.pow(2, l.height) - 1) 
        floatLeft(l.min, mergeChildren(l.left, l.right), r)
      else if (r.size < math.pow(2, r.height) - 1)
        floatRight(r.min, l, mergeChildren(r.left, r.right))
      else if (r.height < l.height)
        floatLeft(l.min, mergeChildren(l.left, l.right), r)
      else floatRight(r.min, l, mergeChildren(r.left, r.right))

    def bubbleRootDown(h: PersistentHeap[A]): PersistentHeap[A] = 
      if (h.isEmpty) PersistentHeap.empty
      else PersistentHeap.bubbleDown(h.min, h.left, h.right)

    if (isEmpty) fail("An empty HeapF.")
    else bubbleRootDown(mergeChildren(left, right))
  }

  /**
   * Fails with given message.
   */
  def fail(m: String) = throw new NoSuchElementException(m)
}

case class Branch[A <% Ordered[A]](min: A, left: PersistentHeap[A], right: PersistentHeap[A], size: Int, height: Int) extends PersistentHeap[A] {
  def isEmpty: Boolean = false
}

case object Leaf extends PersistentHeap[Nothing] {
  def min: Nothing = fail("An empty HeapF.")
  def left: PersistentHeap[Nothing] = fail("An empty HeapF.")
  def right: PersistentHeap[Nothing] = fail("An empty HeapF.")
  def size: Int = 0
  def height: Int = 0
  def isEmpty: Boolean = true
}

object PersistentHeap {

  def main(args: Array[String]): Unit = {
    var h = fromArray(Array(4,1,8,0,2))  
    
    //Heap-Sort
    
    while(!h.isEmpty){
      println(h.min)
      h = h.remove
    }
  }
  
  /**
   * Returns an empty HeapF.
   */
  def empty[A]: PersistentHeap[A] = Leaf

  /**
   * A smart constructor for HeapF's branch.
   */
  def make[A <% Ordered[A]](x: A, l: PersistentHeap[A] = Leaf, r: PersistentHeap[A] = Leaf): PersistentHeap[A] = 
    Branch(x, l, r, l.size + r.size + 1, math.max(l.height, r.height) + 1)

  /**
   * Creates a new HeapF from given sorted array 'a'.
   *
   * Time - O(n)
   * Space - O(log n)
   */
  def fromSortedArray[A <% Ordered[A]](a: Array[A]): PersistentHeap[A] = {
    def loop(i: Int): PersistentHeap[A] = 
      if (i < a.length) PersistentHeap.make(a(i), loop(2 * i + 1), loop(2 * i + 2))
      else PersistentHeap.empty

    loop(0)
  }

  /**
   * Creates a new HeapF from given array 'a'.
   *
   * Time - O(n)
   * Space - O(log n)
   */
  def fromArray[A <% Ordered[A]](a: Array[A]): PersistentHeap[A] = {
    def loop(i: Int): PersistentHeap[A] = 
      if (i < a.length) PersistentHeap.bubbleDown(a(i), loop(2 * i + 1), loop(2 * i + 2))
      else PersistentHeap.empty

    loop(0)
  }


  /**
   * Bubbles given HeapF ('x', 'l', 'r') up.
   *
   * Time - O(1)
   * Space - O(1)
   */
  private[PersistentHeap] def bubbleUp[A <% Ordered[A]](x: A, l: PersistentHeap[A], r: PersistentHeap[A]): PersistentHeap[A] = (l, r) match {
    case (Branch(y, lt, rt, _, _), _) if (x > y) => 
      PersistentHeap.make(y, PersistentHeap.make(x, lt, rt), r)
    case (_, Branch(z, lt, rt, _, _)) if (x > z) => 
      PersistentHeap.make(z, l, PersistentHeap.make(x, lt, rt))
    case (_, _) => PersistentHeap.make(x, l, r)
  }

  /**
   * Bubbles given HeapF ('x', 'l', 'r') down.
   *
   * Time - O(log n)
   * Space - O(log n)
   */
  private[PersistentHeap] def bubbleDown[A <% Ordered[A]](x: A, l: PersistentHeap[A], r: PersistentHeap[A]): PersistentHeap[A] = (l, r) match {
    case (Branch(y, _, _, _, _), Branch(z, lt, rt, _, _)) if (z < y && x > z) => 
      PersistentHeap.make(z, l, PersistentHeap.bubbleDown(x, lt, rt))
    case (Branch(y, lt, rt, _, _), _) if (x > y) => 
      PersistentHeap.make(y, PersistentHeap.bubbleDown(x, lt, rt), r)
    case (_, _) => PersistentHeap.make(x, l, r)
  }
}