package functional.scala

import scala.annotation.tailrec
import myscala.enriched.collections._

object InsertionSort {

  def While(p: => Boolean)(s: => Unit) { if (p) { s; While(p)(s) } }

  /**
   * Last action of the function is a call to some function, then it is tail recursive.
   * By using tail rec optimisation, it becomes equivalent to iterative version in space efficiency.
   */
  @tailrec
  def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  def sum(a: Int, b: Int)(f: Int => Int): Int = if (a > b) 0 else f(a) + sum(a + 1, b)(f)

  def sumSquares(a: Int, b: Int): Int = sum(a, b)(x => x * x)

  def main(args: Array[String]): Unit = {
    var ar = Array(1, 4, 2, 7, 1, 2)

    println(sumSquares(1, 4))

    println(sumSquares2(1, 4))

    println(sum2(x => x * x)(1, 4))

    val nums = List(1, 2, 3, 4, 5, 6, 7, 8)
    println(filter(nums, modN(2))) //Example of currying, modN expects 2 params, one we pass, other automatically passed.
    println(filter(nums, modN(3)))
    
  }

  def sum(f: Int => Int): (Int, Int) => Int = {
    def sumF(a: Int, b: Int): Int = if (a > b) 0 else f(a) + sumF(a + 1, b)
    sumF
  }

  val sumSquares2 = sum(x => x * x) //Currying implies a function returning function.

  def sum2(f: Int => Int)(a: Int, b: Int): Int = {

    @tailrec
    def iter(a: Int, result: Int): Int = {
      if (a > b) result else iter(a + 1, result + f(a))
    }

    iter(a, 0)
  }

  def filter(xs: List[Int], p: Int => Boolean): List[Int] =
    if (xs.isEmpty) xs
    else if (p(xs.head)) xs.head :: filter(xs.tail, p)
    else filter(xs.tail, p)
    
  def modN(n: Int)(x: Int) = ((x % n) == 0)

}