package hackerrank.dp

import scala.collection.mutable.ArrayBuffer
import myscala.enriched.collections._
import scala.io.Source

object Mandragora {
  /**
   * Given array A, S=1, P=0
   * By selecting any number from A, you can perform two operations
   * op1: increase S;  S+=1
   * op2: increase P;  P += S * selected number
   * 
   * You have to maximize P.
   * 
   */
  def main(args: Array[String]): Unit = {
//    0 to readInt -1 foreach { testcase =>
//      readInt
//      solve(readLine.split(" ").map(_.toInt).toArray)
//    }
    
    val in = Source.fromInputStream(getClass().getResourceAsStream("Mandragora.in")).bufferedReader()
    val T = in.readLine().toInt
    val out = Source.fromInputStream(getClass().getResourceAsStream("Mandragora.out")).bufferedReader()
    
    0 to T foreach { tetcase=>
      in.readLine()
      val s = in.readLine()
      val res = solve(s)
      
      val expected = out.readLine().toLong
      if(res != expected){
        println(s"$s $res $expected")
      }
    }
  }
  
  def solve(ln:String): Long = {
    /**
     * Partial solution: x op1 performed, y op2 performed (in any order)=> P(x,y)
     * f(x,y, n)=>max = f(x,y, n-x-y)
     *
     * Exhaustive solution:
     * Select combination(unordered) of numbers from A, select permutation of some numbers from rest: Calculate res.
     * 
     * Optimizations:
     * At any point
     * Increasing count of first set => quadratic growth in res.
     * Increasing count of second set => cubic growth in res.
     *    
     *    So its better to first increase first set to some point, then try for second sequence
     *    Now for second sequence, prev number has additive growth, current element has multiplicative growth
     *    So current elt should be greater than prev
     *    
     *    
     */
     var h = map(int(), ln.trim().split(" ")).toArray
     h = h.sortWith(_>_)
     
     var (s,ans, bago) = (0l,0l, 0l)
     val n = h.length
     
     0 to n-1 foreach { i=>
       s += h(i)
       bago = s*(n-i)
       ans = Math.max(bago, ans)
     }

     ans
  }
  
  var sum = ArrayBuffer[Long]()
  
  //increase p from starting from x
  def p(x:Int):Long = {
    
    if(x >= ar.length) return 0
    if(x == 0) return sum(ar.length-1)
    
    return (x+1)*(sum(ar.length-1) - sum(x-1))
  }
  
  var ar:Array[Int] = null
  
}