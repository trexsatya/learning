package myscala.enriched

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer

package object collections {

  def fail(s: String) = throw new Exception(s)
  
  def memoize[A, B](f: A => B) = new (A => B) {
    val cache = scala.collection.mutable.Map[A, B]()
    def apply(x: A): B = cache.getOrElseUpdate(x, f(x))
  }

  def map(f: String => Int, strings: Traversable[String]): Traversable[Int] = {
    return strings.map(f)
  }

  def array[A](f: Int => A, range: Range): Buffer[A] = {
    var ar = ArrayBuffer[A]()
    range.start to range.end foreach { i =>
      ar += f(i)
    }
    return ar
  }

  def map(f: Int => (Int, Int), range: Range): Map[Int, Int] = {
    var ar = Map[Int, Int]()
    if (range.isInclusive) {
      range.start to range.end foreach { i =>
        ar += f(i)
      }
    } else {
      range.start until range.end foreach { i =>
        ar += f(i)
      }
    }
    return ar
  }

  def int(): (String => Int) = {
    return (s: String) => { s.toInt }
  }

  implicit class MapImprovements[A, B](val m: Map[A, B]) {

    def update(k: A)(f: B => B) = m.updated(k, f(m(k)))

  }

  implicit class MatrixImprovements[A](val m: Buffer[Buffer[A]]) {
    val mx = (m flatMap { r => r } map(_.toString().length()) max) +1
    
    def dump() = {
      m foreach { r =>
        r foreach { c=>
          print(s"$c".padTo(mx, ' '))
        }
        println
      }
    }
  }

  implicit class ImprovedRange(range: Range) {
    def map(f: Int => Int): Array[Int] = {
      var ar = ArrayBuffer[Int]()
      if (range.isInclusive) {
        range.start to range.end foreach { i =>
          ar += f(i)
        }
      } else {
        range.start until range.end foreach { i =>
          ar += f(i)
        }
      }
      return ar.toArray
    }

    def map(f: Int => (Int, Int)): Map[Int, Int] = {
      var ar = Map[Int, Int]()
      if (range.isInclusive) {
        range.start to range.end foreach { i =>
          ar += f(i)
        }
      } else {
        range.start until range.end foreach { i =>
          ar += f(i)
        }
      }
      return ar
    }
  }

  implicit class ArrayImprovements[A](val m: Array[A]) {
    val mxLen = (m map(_.toString.length) max) +2
    def dump() = {
      0 to m.length-1 foreach { x =>
        print(s"$x".padTo(mxLen, ' '))
      }
      println
      0 to m.length-1 foreach { x =>
        print(s"|".padTo(mxLen, ' '))
      }
      println      
      m foreach { x =>
        print(s"$x".padTo(mxLen, ' '))
      }
      println
    }

    def walk(a: Int, b: Int)(f: Int => Unit): IndexedSeq[A] = {
      for (i <- a to b) {
        f(i)
      }
      return m
    }

    def apply(a: Int, b: Int): IndexedSeq[A] = {
      if (a >= 0) m.slice(a, a + b)
      else {
        val e = m.length + a + 1
        m.slice(e - b, e)
      }
    }

  }
  
   implicit class BufferImprovements[A](val m: Buffer[A]) {
    val mxLen = (m map(_.toString.length) max) +2 
    
    def dump() = {
      0 to m.length-1 foreach { x =>
        print(s"$x".padTo(mxLen, ' '))
      } 
      println
      0 to m.length-1 foreach { x =>
        print(s"|".padTo(mxLen, ' '))
      }
      println
      m foreach { x =>
        print(s"$x".padTo(mxLen, ' '))
      }
      println
    }
  }
}

