package dp.flavours.optimizations
import myscala.enriched.collections._
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer

object EditDistance {
  /**
   * Edit Distance: Minimum number of differences
   * Given: String A,B
   * To convert A -> B, we can either Insert, Delete or Replace operation on any character of A.
   * Give minimum number of such operations.
   */

  def main(args: Array[String]): Unit = {
    println("Edit Distance :")
    editDistanceDP("ACGCATCA", "ACTGATTCA")

    println("\nLongest common subsequence:")
    longestCommonSubsequence("XMJYAUZ", "MZJAWXU")

    println("\nLongest common substring:")
    longestCommonSubstring("XMJZYAUZ", "XMJZJAWXU")

    println("\nLongest increasing subsequence:")
    longestPalindromicSubsequence("BBABCBCAB")

  }

  /**
   * Consider the last operation that would be performed either I/D/R, if we had found the optimal solution.
   * f(m,n) = f(m-1,n-1) + cost_match(m,n )
   * 			or  f(m-1, n ) + cost_indel(A[m])
   * 			or	f(m  ,n-1) + cost_indel(B[n])
   *      Of course we would take select one with the minimum.
   */
  def editDistanceRec() = {

  }

  val MATCH = 0; val INSERT = 1; val DELETE = 2

  val INF = 100000

  def editDistanceDP(A: String, B: String) = {
    val matches = (a: Char, b: Char) => if (a == b) +2 else -3
    val d = -2 //penalty for insertion/deletion
    val (m, n) = (A.length, B.length)

    object state {
      def apply(c: Int, o: Int): state = { new state(c, o) }
    }

    var sol = Buffer.tabulate[state](m + 1, n + 1)((i, j) =>
      if (i == 0 && j > 0) state(d * j, INSERT)
      else if (j == 0 && i > 0) state(d * i, DELETE)
      else state(0, -1))

//    sol foreach println
      
    var cost = Array(0, 0, 0)

    1 to m  foreach { i =>
      1 to n  foreach { j =>
        cost(MATCH) = sol(i - 1)(j - 1).cost + matches(A(i-1), B(j-1)) //Both characters consumed. //Match or substitute
        cost(INSERT) = sol(i)(j - 1).cost + d //One character inserted in A
        cost(DELETE) = sol(i - 1)(j).cost + d //One character deleted from A

        sol(i)(j).cost = cost(MATCH); sol(i)(j).operation = MATCH

        INSERT to DELETE foreach (op =>
          if (cost(op) > sol(i)(j).cost) {
            sol(i)(j).cost = cost(op); sol(i)(j).operation = op
          })
      }
    }

    
    println(sol(m - 1)(n - 1))

//    computeAlignment(A, B, sol, m - 1, n - 1)

    var AlignmentA = ""
    var AlignmentB = ""
    var i = m-1
    var j = n-1

    while (i > 0 && j > 0) {
      if (i > 0 && j > 0 && sol(i)(j).cost == (sol(i - 1)(j - 1).cost + matches(A(i), B(j)))) {
        AlignmentA = A(i) + AlignmentA
        AlignmentB = B(j) + AlignmentB
        i -= 1
        j -= 1
      } else if (i > 0 && sol(i)(j).cost == sol(i - 1)(j).cost + d) {
        AlignmentA = A(i) + AlignmentA
        AlignmentB = "-" + AlignmentB
        i = i - 1
      } else {
        AlignmentA = "-" + AlignmentA
        AlignmentB = B(j) + AlignmentB
        j = j - 1
      }
    }
    
//    sol foreach println
    sol.dump()
    
    println(AlignmentB)
    println(AlignmentA)
    
  }

  class state(var cost: Int, var operation: Int) {
    override def toString() = new String((cost, operation) + "")
  }

  def computeAlignment(A: String, B: String, dp: Buffer[Buffer[state]], i: Int, j: Int): Unit = {
    val parent = dp(i)(j).operation;

    if (parent == -1) { print(if (A(i) == B(j)) "M" else "S"); println; return }
    if (parent == MATCH) { computeAlignment(A, B, dp, i - 1, j - 1); print(if (A(i) == B(j)) "M" else "S"); return }

    if (parent == INSERT) { computeAlignment(A, B, dp, i, j - 1); print("I"); return }
    if (parent == DELETE) { computeAlignment(A, B, dp, i - 1, j); print("D"); return }
  }

  def optimalAlignment = {

  }

  def substringApproximateMatching(pattern: String, text: String) = {
    var dp = Buffer.tabulate(pattern.length(), text.length())((i, j) => Buffer(INF, -1)) //(cost,parent)

    val initZeroethRow = (col: Int) => { dp(0)(col)(0) = 0; }

    val initZeroethColumn = (row: Int) => { dp(row)(0)(0) = row; if (row == 0) dp(row)(0)(1) = DELETE }

    val matches = (a: Char, b: Char) => if (a == b) 0 else 1
    val indel = (c: Char) => 1

    //    computeCost(pattern, text, dp)(initZeroethRow)(initZeroethColumn)(matches)(indel)

    var i = pattern.length() - 1; var j = 0
    1 to text.length() - 1 foreach { k => if (dp(i)(k)(0) < dp(i)(j)(0)) j = k }

    println(dp(i)(j))
  }

  def longestCommonSubsequence(A: String, B: String) = {

    //Just make substitution more expensive than INSERTION and deletion
    val matches = (a: Char, b: Char) => if (a == b) 0 else 2

    editDistanceDP(A, B)

    /**
     * Another solution
     */
    /**
     * What will be the last element of our solution:
     * Last character of A or B or both.
     */
    val (m, n) = (A.length, B.length)
    val dp = Buffer.fill(m + 1, n + 1)(0)

    1 to m - 1 foreach { i =>
      1 to n - 1 foreach { j =>
        if (A(i) == B(j)) dp(i)(j) = dp(i - 1)(j - 1) + 1 //consumed both characters
        else dp(i)(j) = math.max(dp(i - 1)(j), dp(i)(j - 1))
      }
    }

    println(dp(m - 1)(n - 1))
  }

  def longestCommonSubstring(A: String, B: String) = {
    val (m, n) = (A.length, B.length)
    val dp = Buffer.fill(m + 1, n + 1)(0)

    var z = 0
    var ret = Set[String]()

    0 to m - 1 foreach { i =>
      0 to n - 1 foreach { j =>
        if (A(i) == B(j)) {
          if (i == 0 || j == 0) dp(i)(j) = 1
          else dp(i)(j) = dp(i - 1)(j - 1) + 1 //consumed both characters

          if (dp(i)(j) > z) {
            z = dp(i)(j)
            ret = Set(A.substring(i - z + 1, i + 1))
          } else if (dp(i)(j) > z) ret += A.substring(i - z + 1, i + 1)
        } else dp(i)(j) = 0 //None of two consumed
      }
    }
    println(ret)
  }

  def longestPalindromicSubsequence(A: String) = {

    println("\nLongest palindromic subsequence:")
    longestCommonSubsequence(A, A.reverse)

    /**
     * Another solution
     */
    val n = A.length
    var dp = Buffer.fill(n + 1, n + 1)(0)

    0 to n foreach { i => dp(i)(i) = 1 }

    2 to n foreach { subl => //substring length
      0 to n - subl foreach { i =>

        val j = i + subl - 1 //last character of expected palindrome

        if (A(i) == A(j)) {
          if (subl == 2) dp(i)(j) = 2
          else dp(i)(j) = dp(i + 1)(j - 1) + 2 //both characters consumed
        } else dp(i)(j) = math.max(dp(i)(j - 1), dp(i + 1)(j)) //one of two characters consumed, whichever leaves us in better state.
      }
    }

    println(dp(0)(n - 1))
  }

  def longestIncreasingSubsequence(ar: Buffer[Int]) = {

    //LCS of ar and ar.sorted
  }

}