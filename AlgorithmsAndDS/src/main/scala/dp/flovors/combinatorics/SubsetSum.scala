package dp.flovors.combinatorics

object SubsetSum {
  /**
   * Given subset. Considering all the subsets of it, is there a subset which sums up to K.
   * 
   * Consider smaller set and smaller K.
   * 				f(size of set, k) -> Boolean
   * 			  f(   1       , k) -> set(1) == k
   * 				f(   n       , k) -> f(n-1,k) or (set(n) == k) or f(n-1,k-set(n))
   * O(N*K)
   * 
   * PRUNING: For every k < sum(negative elements in set) and k > sum(positive elements in set) 
   * 				f( set, k) = false, So modify original K -> sum(+ve elements in set) - sum(-ve elements in set)
   */
  /**
   * Reduction of SubSetSum problem:
   * PARTITION: Given a set, can we divide into two halves, such that both have equal sums.
   * REDUCTION:
   * 				SUBSET-SUM(set, sum)
   * 				Add   sum(set) + sum to set
   * 				Add 2*sum(set) - sum to set
   * 				Suppose solution to SUBSET-SUM(set, sum) -> sol => sum(sol) = sum
   * 				Add 2*sum(set) - sum to     sol => P1
   * 				Add   sum(set) + sum to set-sol => P2
   * 		Clearly: sum(P1) = sum(P2) = 2*sum(set)
   * 
   * REVERSE-REDUCTION:
   * 				Given P1 + P2, and sum(P1) = sum(P2) { suppose =2*sum(set) }
   * 				Since (2*sum(set) - sum) + (sum(set) + sum) = 3*sum(set),
   * 				These two must be in different partitions. 
   * 				
   * Given a set, Does, there exist a subset whose sum is exactly half of the sum(set)
   * i.e. SUBSET-SUM(set, sum(set)/2) 
   * 
   * REDUCTION to K-SUM:
   * 
   */
  
  def main(args: Array[String]): Unit = {
    
  }
}