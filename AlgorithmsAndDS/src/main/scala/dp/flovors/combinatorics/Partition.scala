package dp.flovors.combinatorics
import myscala.enriched.collections._

object Partition {
  /**
   * PARTITION WITHOUT REARRANGEMENTS:
   * Given an array A and a number k.
   * Divide A into k sub-arrays, such that you have to minimise the maximum sum among these sub-arrays.
   * e.g A = {1,2,3,4,5,6,7,8,9} k=3
   * should be divided in {{1,2,3,4,5},{6,7},{8,9}}  
   */
  
  def main(args: Array[String]): Unit = {
    
  }
  /**
   * Let's see this situation as placing k-1 dividers in the array.
   * Think about placing the last divider between i-th and next to i-th
   * So, the cost would be cost of last partition + cost of largest partition formed to the left of i.
   * 
   * f(n,k) = min ( max( f(i,k-1), sum(elements from i+1 to n) for i <- 1 to n
   * 
   */
  
}