package dp.flovors.combinatorics
import myscala.enriched.collections._

object MakeSum {
  /**
   * Given set e.g. {1,3,4} and S e.g S=5 
   * Find the number of ways in which
   * elements of this set can be combined to make sum S.
   * Duplicates allowed.e.g. ans for above is 6.
   */
  def main(args: Array[String]): Unit = {
    /**
     * Let solution be S = x1 + x2 + x3 + x4
     * Choices for x1 are [1,3,4]
     * map these choices to subproblems {f(S-1), f(S-3), f(S-4)} 
     * 
     * We know that for f(1),f(3) and f(4) answer is 1.
     */
    
    
  }
   
}