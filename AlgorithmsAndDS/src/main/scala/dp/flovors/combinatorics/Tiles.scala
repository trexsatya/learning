package dp.flovors.combinatorics

object Tiles {
  /**
   * Given a 2xn board, in how many ways can we fill this board with 2x1 tiles.
   * Try to reduce problem space.
   * We can place tile vertically, then problem reduces to f(n-1)
   * We can place tile horizontally, to complete the height we need to put another tile,
   * problem reduces to f(n-2) => f(n) = f(n-1) + f(n-2)
   * 
   * What if we are given two types of tiles, one 2x1 and other L-shaped 
   * f(n) = f(n-1) + f(n-2) + g(n-2) + h(n-2), since g(n) = h(n) by symmetry
   * g(n) = f(n-1) + h(n-1) = f(n-1) + g(n-1), since g(n) = h(n) by symmetry
   * 
   */
  
  /**
   * Given 3xn board, in how many ways, can we fill this board with 2x1 tiles.
   * So we can lay tiles horizontally(H) or vertically(V).
   * Try plucking up one,two, three elements from optimal solution to reach down to subproblems.
   * 
   * Suppose solution is 
   * f(n) = f(n-2) {for structure H}
   *                              H
   *                              H
   *       + 2g(n-1) {for H at one corner top-left/bottom-left}
   *       Now to cover up the half of tile on corner
   *       We can either place a V tile below => T(n-2)
   *       Or we can place two H's one below other, and then we are forced to place H next to tile at
   *       corner which implies similar subproblem P(n-3)
   *       i.e. g(n-1) = f(n-2) + g(n-3)
   *       
   *       
   * Solving these two equations gives us (Add equations, replace n -> n-2 in first, and solve) 
   * f(n) = 4f(n-2) - f(n-4)       
   * f(0) = 0
   */                     
  
  def main(args: Array[String]): Unit = {
    
  }
  
  
  
}