package recursion.flavors.decisions

import scala.annotation.tailrec

class Backtracking{
  
  def process() = {
    
  }
}

object BacktrackingExamples {
  /**
   * PARTIAL EXPLORATION OF PERMUTATION/DECISION-TREE
   * Idea: In permutation/combination all choices were recurred.
   * In backtracking, next choice is recurred, only if this choice does not give desired result on return. 
   * 
   */
  def main(args: Array[String]): Unit = {
    println
  }
  
  /**
   * Place 8 queens on chess board, such that no queen can kill other.
   * 
   */
  def queens(grid:Array[Array[Int]], col: Int): Boolean = {
    
    if(col >= grid(0).length) return true //reached the end
    
    val choices = 0 to grid.length
    
    val isSafe = (r:Int, c:Int) => true
    val placeQueen = (r:Int, c:Int) => {}
    val removeQueen = (r:Int, c:Int) => {}
      
    choices foreach { row =>
      
      if(isSafe(row,col)) {
        placeQueen(row,col)
        
        if(queens(grid, col+1)) return true    
        
        removeQueen(row,col)
      }
    }
    
    return false
  }
  
  /**
   * Complete Search Recursion
   */
  def queens(n: Int): List[List[Int]] = {
    def isSafe(col: Int, queens: List[Int], delta: Int): Boolean = true

    def placeQueens(k: Int): List[List[Int]] = 
      if (k == 0) List(List()) 
      else for { queens <- placeQueens(k - 1) 
                  column <- List.range(1, n + 1) 
                if isSafe(column, queens, 1) } yield column :: queens 
    placeQueens(n) 
  }

  val nextCell: ((Int,Int)) => (Int,Int) = 
    cell => {
      var r=cell._1; var c=cell._2
      
      if(r+1 > 8 && c+1 > 8) null //reached end
      else {
        c += 1
        if(c >8 ) {c = 0; r+=1} //reached last column
        (r,c)
      }
   };
   
   
  /***
   * In 9x9 grid, place 1 to 9 such that, no duplicate occurs in each row, column and block of 3x3. 
   */
  def sudoku(grid:Array[Array[Int]], curCell:(Int,Int)): Boolean = {
    val isSafe = (cell:(Int,Int), n:Int) => true
    
    if(curCell == null) return true //reached the end
    
    val choices = 1 to 9
    choices foreach { n=>
      
      if(isSafe(curCell,n)) {
        grid(curCell._1)(curCell._2) = n //place
        
        if(sudoku(grid, nextCell(curCell))) return true
        
        grid(curCell._1)(curCell._2)  = 0 //reset
      }
    }
    
    return false
  }
}