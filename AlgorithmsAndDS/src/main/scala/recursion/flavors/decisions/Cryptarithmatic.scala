package recursion.flavors.decisions
import scala.language.dynamics 
import scala.collection.mutable.HashSet
import scala.collection.mutable.Set
import scala.collection.mutable.HashMap
import scala.collection.mutable.Map

object Cryptarithmatic {
  /**
   * Assign digits to letters such that, given condition is satisfied.
   * e.g. SEND + MORE = MONEY
   */
  class Puzzle{
    var operand1 = "send"
    var operand2 = "more"
    var res = "money"
    
    def allLetters():List[Char] = {
      var set:Set[Char] = HashSet()
      operand1 foreach {  c => set += c } 
      operand2 foreach {  c => set += c } 
      res foreach {  c => set += c } 

      set.toList.sorted
    }
  }
  
  def main(args: Array[String]): Unit = {
      var time = System.currentTimeMillis()
      println(clist)

      for(i <- 0 to 9 ){
        usedDigits = Set[Int]() 
        
        if(dumbSolver(i, clist)){
           println(assignment)
           println(System.currentTimeMillis() - time)
           println(cnt)
           return
        }
      }
  }
  
  
  var puzzle = new Puzzle()
  val clist =  puzzle.allLetters()
  var assignment:Map[Char,Int] = HashMap()
  var usedDigits = Set[Int]()
    
  val isSafe = (choice:Int) => {
//    if(pruned(a)) false
    
    !usedDigits.contains(choice)
  }
  
  var cnt = 0
  def dumbSolver(indx:Int, lettersToAssign:List[Char]):Boolean ={
    if(assignment.size == clist.size) 
                return puzzleSolved(assignment)

    cnt += 1
    val choices = 0 to 9
    
    choices filter isSafe foreach  { choice=>
      
        assignment += (lettersToAssign(indx) -> choice)
        usedDigits += choice
        
        if(dumbSolver(indx+1, lettersToAssign)) return true
        
        assignment -= lettersToAssign(indx)
        usedDigits -= choice
    }
    
    false
  }

  def puzzleSolved(assignment: Map[Char,Int]):Boolean = {
    getValue(puzzle.operand1) + getValue(puzzle.operand2) == getValue(puzzle.res) 
  }

  def getValue(a:String ):Int = {
		a.foldLeft(0){ (res,c) => 10 * res + assignment(c) }
	}
  
  def pruned(assign: (Char, Int)): Boolean = {
   false
  }
}