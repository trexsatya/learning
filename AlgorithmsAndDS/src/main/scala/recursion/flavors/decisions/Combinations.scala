package recursion.flavors.decisions

object Combinations {
  def main(args: Array[String]): Unit = {
    
    f("","abc")
    
    bitmaskSubset("abc")
  }

  def f(subsetSoFar:String, rest:String): Unit = {
    if(rest == "") println(subsetSoFar)
    else {
            // included
         f(subsetSoFar+rest(0), rest.substring(1))
            // not included
         f(subsetSoFar        , rest.substring(1))
    }
  };
  
  /**
   * Can be used if max number of elements in set is 64, 
   */
  def bitmaskSubset(str:String) = {
    val n:Long = str.length()
    
    0 to (1<<n)-1 foreach { subset=>
      var substr=""
      0 to 63 foreach { indx=>
        if((subset & 1l << indx) != 0) substr += str(indx)
      }
      println(substr)
    }
  }
}