package recursion.flavors.decisions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class CryptarithmaticJava {

	static class Puzzle{
		String a = "send", b ="more", c ="money";

		public List<Character> allLetters() {
			Set<Character> set = new HashSet<>();
			for(char c: a.toCharArray()) set.add(c);
			for(char c: b.toCharArray()) set.add(c);
			for(char c: c.toCharArray()) set.add(c);
			
			return set.stream().sorted().collect(Collectors.toList());
		}
		
	}
	
	Puzzle puzzle = new Puzzle();
	Map<Character, Integer> assignments = new HashMap<Character, Integer>();
	Set<Integer> usedDigits = new HashSet<>();
	List<Character> clist = puzzle.allLetters();
	
	public static void main(String[] args) {
		new CryptarithmaticJava().solve();
	}
	
	long cnt = 0;
	
	private void solve() {
		 long time = System.currentTimeMillis();
				 
		 System.out.println(clist);
		 for (int i = 0; i < 10; i++) {
			usedDigits.clear();
			
			if (solve(i, clist)) {
//				printSol(assignments);
				System.out.println(assignments);
				System.out.println(System.currentTimeMillis()- time);
				System.out.println(cnt);
				return;
			}
		 }		
	}

	private boolean solve(int i, List<Character> lettersToAssign) {
		if(assignments.size() == lettersToAssign.size()) return puzzleSolved();
		
		cnt++;
		for (int n = 0; n < 10; n++) {
			if(!usedDigits.contains(n)){
				
				assignments.put(lettersToAssign.get(i), n);
				usedDigits.add(n);
				
				if(solve(i+1, lettersToAssign)) return true;
				
				assignments.remove(lettersToAssign.get(i));
				usedDigits.remove(n);
			}
		}
		return false;
	}

	private  boolean puzzleSolved() {
		int av = getValue(puzzle.a);
		int bv = getValue(puzzle.b);
		int resultv = getValue(puzzle.c);
		return av + bv == resultv;
	}

	private int getValue( String a) {
		int res = 0;
		for (int i = 0; i < a.length(); i++) {
			res = 10 * res + assignments.get(a.charAt(i));
		}
		return res;
	}

	private static void printSol(Map<Character, Integer> map) {
		Set<Entry<Character, Integer>> set = map.entrySet();
		for (Entry<Character, Integer> entry : set) {
			System.out.println(entry.getKey() + " = " + entry.getValue());
		}
	}
}
