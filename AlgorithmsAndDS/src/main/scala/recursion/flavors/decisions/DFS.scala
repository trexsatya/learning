package recursion.flavors.decisions

object DFS {
  
  def main(args: Array[String]): Unit = {
    
  }
  
  var visited = Array[Boolean]()
  
  def neighbors(vertexU: Int): Array[Int] = {
    Array()
  }
  
  def dfs(vertexU:Int): Unit = {
    if(visited(vertexU)) return
    
    visited(vertexU) = true
    
    neighbors(vertexU) filter(!visited(_)) foreach { vertexV=>
      dfs(vertexV)
    }
  }
}