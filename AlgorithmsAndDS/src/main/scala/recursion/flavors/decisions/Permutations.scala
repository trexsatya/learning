package recursion.flavors.decisions

import scala.collection.mutable.Stack

object flavors {
  
  def main(args: Array[String]): Unit = {
    println("permutations")
      f("", "satya")  
      
     val dup =  stk.groupBy(identity).collect { case (x,ys) if ys.size > 1 => x }
//     println(dup)
    
  }

  /**
   * Can use stack to eliminate duplicates. Can use set to eliminate duplicates.
   */
  var stk = Stack[String]()
  
  def f(permutedSoFar:String, rest: String): Unit = {
    
    if(rest.equals("")) { if(stk.isEmpty || !stk.top.equals(permutedSoFar))stk.push(permutedSoFar); println(permutedSoFar) }
    else
      for(i <- 0 until rest.length){
        val remaining = rest.substring(0,i) + rest.substring(i+1) 
        //All except char at i
        //to handle duplicates -> continue if(rest(i)) is in rest
        f(permutedSoFar+rest(i),  remaining)
      }
  }
}