package codelibrary;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FunWithBits {

//	static void m(long... a){
//		System.out.println("integer");
//	}
	
	static void m(long a){
		System.out.println("long ");
	}
	
	static void m(Long a){
		System.out.println("Long");
	}
	
//	static void m(Object a){
//		System.out.println("object");
//	}

	public static void main(String[] args) {

		Integer b = 12;
		
		//byte -> Byte -> Object
		
		//int ->(boxing) Integer ->(widening) Object
	
		
		//primitive ->(widen into primitives) |-> Boxing -> Widening |-> varagrs -> (widen into primitives) 
		m(b);
		
		System.out.println( (2&1) == 0);
		System.out.println( (3&1) == 0);
		
		int set = createSet(3, 12, 10);

		printSet(set);

		int n = 3;
		Set<Integer> subset = new HashSet<>();
		for (int i = 1; i < (1 << n); i++) {
			// iterate over all the subsets of the i-th subset
//			print(i);
			System.out.println("subset->"+membersInSet(i));
			
			for (int ii = i; ii > 0; ii = (ii - 1) & i) {
				// generate the subset induced by i2
				
				subset.add(ii);
		         printSet(ii);
		         
			}
		}
		System.out.println();
		subset.forEach(s-> printSet(s));

		// int can be used to represent a set/subset of 32 members.
		int x = 0;
		printBinary(x);

		// Set i-th bit
		x |= (1 << 30);
		printBinary(x);

		// Clear i-th bit
		x &= ~(1 << 30);
		printBinary(x);

		// Flip i-th bit
		x ^= (1 << 30);
		printBinary(x);

		// Test if j-th bit is set
		testBit(x, 30);

		// Flip i-th bit
		x ^= (1 << 30);
		printBinary(x);

		// Test if j-th bit is set
		testBit(x, 30);

		x = 0;
		x |= 1 << 5;
		x |= 1 << 8;
		x |= 1 << 9;
		x |= 1 << 30;
		printBinary(x);

	}

	private static void printSet(int set) {
		System.out.println(membersInSet(set));
	}

	private static void testBit(int x, int i) {
		System.out.println((x & 1 << i) != 0);

	}

	private static void printBinary(int x) {
		System.out.println(String.format("%32s", Integer.toBinaryString(x)).replace(' ', '0'));
	}

	public static int createSet(int... members) {
		int set = 0;
		for (int m : members)
			set = includeNthMemberIntoSet(m, set);
		return set;
	}

	public static List<Integer> membersInSet(int set) {
		List<Integer> list = new ArrayList<>();

		for (int i = 0; i < 31; i++) {
			if (nThMemberIsInSet(i, set))
				list.add(i);
		}
		return list;
	}

	public static IntStream allSubsets(int set) {
		int n = membersInSet(set).size();
		return IntStream.range(0, 1 << n);
	}

	private static int unionOfSets(int a, int b) {
		return a |= b;
	}

	private static int includeNthMemberIntoSet(int n, int set) {
		return set |= 1 << n;
	}

	private static boolean nThMemberIsInSet(int n, int set) {
		return (set & 1 << n) != 0;
	}
}
