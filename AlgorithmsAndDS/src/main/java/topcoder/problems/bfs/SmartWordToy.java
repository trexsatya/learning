package topcoder.problems.bfs;

import static com.satya.utils.Utils.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;

import com.satya.common.assertions.BehavioralAssertions;

import topcoder.problems.common.TestCaseFileReader;

public class SmartWordToy {
	
	public static void main(String[] args) {
		SmartWordToy toy = new SmartWordToy();
		
		List<Long> timesTaken = new ArrayList<>();
		
		TestCaseFileReader.read(toy.getClass(), s-> s.length() > 0, (in, expected)->{

			String[] splits = splitByCommaExceptEnclosedInCurlyBrackets(in);
			
//			System.out.println(TestCaseFileReader.listOfStrings(splits[2]));
			String s = trimmedString(splits[0]);
			String f = trimmedString(splits[1]);
			List<String> forbids = listOfStrings(splits[2]);

			long strt = nanos();
			
			int minPresses = toy.minPresses(s, 
											 f,
											forbids
											);
			timesTaken.add(nanos() - strt);
			
//			System.out.println(minPresses);
			
			Assert.assertEquals(expected, minPresses+"");
		});
		
		double avgAsDouble = timesTaken.stream().mapToLong(l-> l).average().getAsDouble();
		System.out.println(TimeUnit.NANOSECONDS.toMillis((long)avgAsDouble));
		
	}
	
	private static char nextCircularChar(char c) {
		if(c == 'z') return 'a';
		return Character.valueOf((char)(c+1));
	}
	
	private static char prevCircularChar(char c) {
		if(c == 'a') return 'z';
		return Character.valueOf((char)(c-1));
	}

	Map<String, Integer> best;
	Map<String, Boolean> bad ;
	
	int minPresses(String s, String f, List<String> forbid) {
		best =new HashMap<>();
		bad =new HashMap<>();
		
		best(s,0);
		
		Queue<String> q = new LinkedList<>();
		q.offer(s);
		
		for(String forbiddenSeq: forbid){
			String[] forbids = forbiddenSeq.split(" ");
			for(int a=0;a<forbids[0].length();a++)
				for(int b=0;b<forbids[1].length();b++)
					for(int c=0;c<forbids[2].length();c++)
						for(int d=0;d<forbids[3].length();d++)
							
							bad(forbids[0].charAt(a)+""+
								forbids[1].charAt(b)+
								forbids[2].charAt(c)+
								forbids[3].charAt(d)
													, true);
			
		}
		
		while(!q.isEmpty()){
//			System.out.println(q);
			
			String qe = q.poll();
			
			int cost = best(qe);
			for(int i=0; i<4; i++){
				StringBuilder sb = new StringBuilder(qe);
				char x = sb.charAt(i);
				
				sb.setCharAt(i, nextCircularChar(x));
				if(!bad(sb.toString()) && best(sb.toString()) == null){
					best(sb.toString(),cost+1);
					q.offer(sb.toString());
				}
				
				sb.setCharAt(i, prevCircularChar(x));
				if(!bad(sb.toString()) && best(sb.toString()) == null){
					best(sb.toString(),cost+1);
					q.offer(sb.toString());
				}
//				sb.setCharAt(i, x);
			}
		}
		Integer ans = best(f);
		best = null;
		bad = null;
	    return ans != null ? ans : -1;
	    
	}

	private void bad(String s, boolean b) {
		bad.put(s, b);
	}
	
	private Boolean bad(String s) {
		return bad.getOrDefault(s, false);
	}

	private Integer best(String s) {
		return best.get(s);
	}
	private void best(String s, int v) {
		best.put(s,v);
	}
}
