How to identify BFS problems:
You have to find minimum number of steps.
You have physical or logical states/positions
You have a starting point
You have to jump through selected neighbours -- STEPs
Cost of these jumps is same (generally 1).
You have  finishing point(s)