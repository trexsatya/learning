package topcoder.problems.bfs;

import static com.satya.utils.Utils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.Assert;

import javaslang.Tuple2;
import topcoder.problems.common.TestCaseFileReader;

public class CaptureThemAll {

	public static void main(String[] args) {
		CaptureThemAll obj = new CaptureThemAll();
		
		List<Long> timesTaken = new ArrayList<>();
		
		TestCaseFileReader.read(obj.getClass(), s-> s.length() > 0, (in, expected)->{
			String[] split = in.split(",");
			String knight = trimmedString(split[0]);
			String rook = trimmedString(split[1]);
			String queen = trimmedString(split[2]);
			
			long start = nanos();
			int steps = obj.fastKnight(knight, rook, queen);
			
			Assert.assertEquals(expected, steps+"");
			timesTaken.add(nanos()-start);
		});
		
		double avgAsDouble = timesTaken.stream().mapToLong(l-> l).average().getAsDouble();
		System.out.println(TimeUnit.NANOSECONDS.toMillis((long)avgAsDouble));
	}
	
	int fastKnight(String knight, String rook, String queen) {
//		print(knight, rook);
		
		int knightToRook = bfs(state(knight), state(rook));
		int knightToQueen = best.getOrDefault(tuple(state(queen)), -1);
		
		int rookToQueen = bfs(state(rook), state(queen));
//		int queenToRook = bfs(state(rook), state(queen)); //No need for this, because essentially queenToRook = rookToQueen
		
//		print(knightToRook, knightToQueen, rookToQueen, queenToRook);
		
		return Math.min(knightToRook+ rookToQueen, knightToQueen+rookToQueen);
		
	}
	
	Tuple2<Integer, Integer> tuple(int[] state){
		return new Tuple2<>(state[0], state[1]);
	}
	
	
	Map<Tuple2<Integer, Integer>, Integer> best;
	
	private int bfs(int[] start, int[] end) {
		best = new HashMap<>();
		best.put(tuple(start), 0);
		
		Queue<Tuple2<Integer,Integer>> q = new LinkedList<>();
		q.add(tuple(start));
		
		while(!q.isEmpty()){
//			System.out.println(q);
			
			Tuple2<Integer, Integer> curr = q.poll();
			Integer cost = best.getOrDefault(curr, 0);
			
			neighbors(curr).stream()
				.filter(nbr-> best.get(nbr) == null)
				.forEach(nbr -> {
					q.offer(nbr);
					best.put(nbr, cost + 1);
				 });
		}
		
//		print(best);
		
		return best.getOrDefault(tuple(end), -1);
	}

	List<Tuple2<Integer,Integer>> neighbors(Tuple2<Integer,Integer> position) {
		int col = position._1(), row = position._2();
		
		List<int[]> states = new ArrayList<>();
		states.add( new int[]{ col-2, row-1} );
		states.add( new int[]{ col-2, row+1} );
		states.add( new int[]{ col+2, row-1} );
		states.add( new int[]{ col+2, row+1} );
		states.add( new int[]{ col-1, row-2} );
		states.add( new int[]{ col+1, row-2} );
		states.add( new int[]{ col-1, row+2} );
		states.add( new int[]{ col+1, row+2} );
		
		return states.stream().
				filter(pos-> pos[0] >= 0 && pos[0] < 8 && pos[1] >= 0 && pos[1] < 8).
					map(pos -> new Tuple2<Integer, Integer>(pos[0],pos[1])).
					collect(Collectors.toList());
	}
	
	public int[] state(String s){
		int col = s.charAt(0) - 'a'; int  row = intValue(s.charAt(1))-1;
		return new int[]{col, row};
	}
}
