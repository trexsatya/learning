package topcoder.problems.bfs;

import static com.satya.utils.Utils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Assert;

import javaslang.Tuple2;
import topcoder.problems.common.TestCaseFileReader;

public class RevolvingDoors {

	public static void main(String[] args) {
		RevolvingDoors obj = new RevolvingDoors();

		List<Long> timesTaken = new ArrayList<>();

		TestCaseFileReader.read(obj.getClass(), s -> s.length() > 0, (in, expected) -> {

			// Read input
			List<String> mazeRows = listOfStrings(in);

			long strt = nanos();

			int result = obj.turns(mazeRows.toArray(new String[] {}));

//			Assert.assertEquals(expected, result + "");

			System.out.println(intValue(expected) == result);
			timesTaken.add(nanos() - strt);
		});

		double avgAsDouble = timesTaken.stream().mapToLong(l -> l).average().getAsDouble();
		System.out.println(TimeUnit.NANOSECONDS.toMillis((long) avgAsDouble));
	}

	int h;
	int w;
	int nd;
	char[][] m;
	boolean[][][] v;
	PQ q;

	static class Entry {
		public int key;
		public int x, y, d;

		public Entry(int key, int a, int b, int c) {
			this.key = key;
			x = a;
			y = b;
			d = c;
		}
	}

	static class PQ {
		ArrayList q = new ArrayList();

		public boolean isEmpty() {
			return q.isEmpty();
		}

		int parent(int i) {
			return (i + 1) / 2 - 1;
		}

		int left(int i) {
			return (i + 1) * 2 - 1;
		}

		int right(int i) {
			return (i + 1) * 2;
		}

		public void add(int key, int x, int y, int d) {
			q.add(null);
			int i = q.size() - 1;
			while (i > 0 && ((Entry) q.get(parent(i))).key > key) {
				q.set(i, q.get(parent(i)));
				i = parent(i);
			}
			q.set(i, new Entry(key, x, y, d));
		}

		public Entry popEntry() {
			Entry e = (Entry) q.get(0);
			q.set(0, q.get(q.size() - 1));
			q.remove(q.size() - 1);

			int i = 0;
			boolean done = false;
			while (!done) {
				int l = left(i), r = right(i);
				int min = i;
				if (l < q.size() && ((Entry) q.get(l)).key < ((Entry) q.get(min)).key) {
					min = l;
				}
				if (r < q.size() && ((Entry) q.get(r)).key < ((Entry) q.get(min)).key) {
					min = r;
				}
				if (min != i) {
					Object temp = q.get(min);
					q.set(min, q.get(i));
					q.set(i, temp);
					i = min;
				} else {
					done = true;
				}
			}
			return e;
		}
	}

	void add(Entry e, int dx, int dy) {
		int x = e.x + dx;
		int y = e.y + dy;
		if (x < 0 || y < 0 || x >= w || y >= h || m[y][x] == '#')
			return;
		int k = e.key;
		int s = e.d;
		if (m[y][x] >= 150) {
			int d = m[y][x] - 150;
			if ((s & 1 << d) == 0) {
				if (dy == 0)
					return;
				k++;
				s ^= 1 << d;
			}
		} else if (m[y][x] >= 130) {
			int d = m[y][x] - 130;
			if ((s & 1 << d) != 0) {
				if (dx == 0)
					return;
				k++;
				s ^= 1 << d;
			}
		}
		q.add(k, x, y, s);
	}

	void add2(Entry e, int dx, int dy) {
		int x = e.x + dx;
		int y = e.y + dy;
		if (x < 0 || y < 0 || x >= w || y >= h || m[y][x] == '#')
			return;
		q.add(e.key, x, y, 0);
	}

	boolean safe(int sx, int sy, int ex, int ey) {
		q = new PQ();
		q.add(0, sx, sy, 0);
		boolean[][] v = new boolean[h][w];
		while (!q.isEmpty()) {
			Entry e = (Entry) q.popEntry();
//			if(e.y < 0 || e.x < 0) continue;
			
			if (v[e.y][e.x])
				continue;
			v[e.y][e.x] = true;
			if (ey == e.y && e.x == ex)
				return true;
			add2(e, 1, 0);
			add2(e, 0, 1);
			add2(e, -1, 0);
			add2(e, 0, -1);
		}
		return false;
	}

	public int turns(String[] map) {
		int max = Stream.of(map).mapToInt(s->s.length()).max().getAsInt();
		
		h = map.length;
		w = map[0].length();
		m = new char[h][max];
		int sx = -1, sy = -1, sd = 0, ex = -1, ey = -1;
		nd = 0;
		for (int i = 0; i < h; i++) {
			char[] ca = map[i].toCharArray();
			for(int j=0;j<ca.length;j++) m[i][j] = ca[j];
			for(int j=ca.length;j<max;j++) m[i][j] = ' ';
		}
		
		for (int i = 0; i < h; i++)
			for (int j = 0; j < w; j++) {
//				if(j >= m[i].length) continue;
		
				if (m[i][j] == 'S') {
					sx = j;
					sy = i;
				} else if (m[i][j] == 'E') {
					ex = j;
					ey = i;
				}}
		for (int i = 0; i < h; i++)
			for (int j = 0; j < w; j++) {
//				if(j >= m[i].length) continue;
				
				if (m[i][j] == 'O') {
					if (m[i - 1][j] == '|')
						sd |= 1 << nd;
					m[i - 1][j] = (char) (130 + nd);
					m[i + 1][j] = (char) (130 + nd);
					m[i][j - 1] = (char) (150 + nd);
					m[i][j + 1] = (char) (150 + nd);
					m[i][j] = '#';
					nd++;
				}}
		v = new boolean[h][w][1 << nd];
		if (!safe(sx, sy, ex, ey))
			return -1;
		q = new PQ();
		q.add(0, sx, sy, sd);
		while (!q.isEmpty()) {
			Entry e = (Entry) q.popEntry();
			if (v[e.y][e.x][e.d])
				continue;
			v[e.y][e.x][e.d] = true;
			if (ey == e.y && e.x == ex)
				return e.key;
			add(e, 1, 0);
			add(e, 0, 1);
			add(e, -1, 0);
			add(e, 0, -1);
		}
		return -1;
	}

}
