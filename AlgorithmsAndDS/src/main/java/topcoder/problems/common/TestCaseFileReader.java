package topcoder.problems.common;


import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class TestCaseFileReader {
	
	
	public static void read(Class<?> cls, Predicate<String> filter, BiConsumer<String, String> consumer) {
		String nm = cls.getSimpleName()+".in";
		System.out.println(nm);
		
		try (Stream<String> stream = Files.lines(Paths.get(cls.getResource(nm).toURI()))){
			stream.filter(filter).forEach(s->{
				String[] splits = s.split("\\t\\t");
				String expectation = splits[1];
				String input = splits[0];
				consumer.accept(input, expectation);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
