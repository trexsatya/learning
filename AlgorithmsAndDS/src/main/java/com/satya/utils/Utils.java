package com.satya.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javaslang.Tuple2;

public class Utils {

	public static long nanos() {
		return System.nanoTime();
	}

	public static String[] splitByCommaExceptEnclosedInCurlyBrackets(String s) {
		/**
		 * , # Matches , \s* # Matches zero or more whitespace characters (?! #
		 * Starts a negative look-ahead assertion [^{}]* # Matches zero or more
		 * characters that are not { or } \} # Matches } ) # Closes the
		 * look-ahead assertion
		 * 
		 */
		return s.split(",\\s*(?![^{}]*\\})");
	}

	public static String[] splitByWhitespaceExceptEnclosedInCurlyBrackets(String s) {
		/**
		 * , # Matches , \s* # Matches zero or more whitespace characters (?! #
		 * Starts a negative look-ahead assertion [^{}]* # Matches zero or more
		 * characters that are not { or } \} # Matches } ) # Closes the
		 * look-ahead assertion
		 * 
		 */
		return s.split("\\s\\s*(?![^{}]*\\})");
	}

	public static List<String> listOfStrings(String str) {
		String itemsStr = str.substring(str.indexOf("{") + 1, str.indexOf("}"));
		String[] split = itemsStr.split(",");

		List<String> list = new ArrayList<>();
		for (String s : split)
			list.add(trimmedString(s));

		return list;
	}

	public static String[] arrayOfStrings(String str){
		return listOfStrings(str).toArray(new String[0]);
	}
	
	public static String trimmedString(String str) {
		String trim = str.trim();
		StringBuilder sb = new StringBuilder(trim);
		if (trim.startsWith("\"") && trim.endsWith("\"")) {
			sb.delete(0, 1);
			sb.delete(sb.length() - 1, sb.length());
		}

		return sb.toString();
	}

	public static void print(Object... args) {
		StringBuilder sb = new StringBuilder();
		for (Object o : args) {
			if (o instanceof int[])
				sb.append(Arrays.toString((int[]) o)).append(" ");
			else
				sb.append(o.toString()).append("  ");
		}

		System.out.println(sb.toString());
	}

	public static int intValue(String s) {
		return Integer.parseInt(s);
	}

	public static int intValue(char s) {
		return Integer.parseInt(s + "");
	}

	public static void print(char[][] matrix) {
		for (char[] ca : matrix) {
			for (int i = 0; i < ca.length - 1; i++)
				System.out.print(ca[i] + ", ");
			System.out.print(ca[ca.length - 1]);
			System.out.println();
		}
	}

	public static void print(int[][] matrix) {
		for (int[] ca : matrix) {
			for (int i = 0; i < ca.length - 1; i++)
				System.out.print(ca[i] + ", ");
			System.out.print(ca[ca.length - 1]);
			System.out.println();
		}
	}

	<X, Y> Tuple2<X, Y> pair(X x, Y y) {
		return new Tuple2<X, Y>(x, y);
	}

	public static List<Integer> listOfIntegers(String str) {
		String itemsStr = str.substring(str.indexOf("{") + 1, str.indexOf("}"));
		String[] split = itemsStr.split(",");

		List<Integer> list = new ArrayList<>();
		for (String s : split)
			list.add(Integer.parseInt(trimmedString(s)));

		return list;
	}

	public static int[] arrayOfInts(List<Integer> list) {
		int[] res = new int[list.size()];
		int i = 0;
		for (Integer x : list)
			res[i++] = x;

		return res;
	}
	
	public static int[] arrayOfInts(String str) {
		String itemsStr = str;
		String[] split = itemsStr.split("\\s+");
		
		if(str.indexOf("{") >= 0 && str.indexOf("{") >= 0) {
			itemsStr = str.substring(str.indexOf("{") + 1, str.indexOf("}"));
			split = itemsStr.split(",");
		}

		List<Integer> list = new ArrayList<>();
		for (String s : split)
			list.add(Integer.parseInt(trimmedString(s)));

		return arrayOfInts(list);
	}

	public static Integer[] arrayOfIntegers(String str) {
		String itemsStr = str.substring(str.indexOf("{") + 1, str.indexOf("}"));
		String[] split = itemsStr.split(",");

		List<Integer> list = new ArrayList<>();
		for (String s : split)
			list.add(Integer.parseInt(trimmedString(s)));

		Integer[] res = new Integer[list.size()];
		int i = 0;
		for (Integer x : list)
			res[i++] = x;

		return res;
	}
	
	public static void twoDimensionalLoop(int x, int y, BiConsumer<Integer,Integer> consumer){
		for(int i=0; i < x; i++){
			for(int j=0; j < y; j++ ){
				consumer.accept(i, j);
			}
		}
	}
	
	public static void loop(int x, Consumer<Integer> consumer){
		for(int j=0; j < x; j++ ){
			consumer.accept(j);
		}
	}
}
