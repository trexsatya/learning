package codecheff.problems.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class TestCasesReader {
	public static interface TestCaseDataSupplier extends Supplier<String> {

		default int[] getInts() {
			String line = get();
			String[] split = line.split("\\s+");

			int[] ints = new int[split.length];
			for (int i = 0; i < split.length; i++)
				ints[i] = Integer.parseInt(split[i]);

			return ints;
		}

		default int getInt() {
			return Integer.parseInt(get());
		}

	}

	public static TestCaseDataSupplier reader() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			TestCaseDataSupplier nextLine = () -> {
				try {
					return reader.readLine();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "";
			};

			return nextLine;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return () -> null;
	}

	public static void forEachTestCase(Class<?> cls, Consumer<List<String>> consumer) {

	}

	public static TestCaseDataSupplier reader(Class<?> cls) {
		URL url = cls.getResource(cls.getSimpleName() + ".in");

		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(url.toURI())));
			TestCaseDataSupplier nextLine = () -> {
				try {
					return reader.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return "";
			};

			return nextLine;
		} catch (FileNotFoundException | URISyntaxException e) {
			e.printStackTrace();
		}

		return () -> null;
	}

}
